package br.com.abee.gestao.Service;

import br.com.abee.gestao.Model.FiltroDados;
import br.com.abee.gestao.Model.FiltroGrafico;
import br.com.abee.gestao.Model.ResponseDashboard;
import br.com.abee.gestao.Model.ResponseGrafico;
import br.com.abee.gestao.Repository.DashboardRepository;
import br.com.abee.gestao.Repository.GraficoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GraficoService {

    @Autowired
    GraficoRepository repository;


    public ResponseGrafico getDevolveDados(FiltroGrafico filtroGrafico) {
        return repository.devolveDados(filtroGrafico);
    }
}
