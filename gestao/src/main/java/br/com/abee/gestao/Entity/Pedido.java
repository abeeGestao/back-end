package br.com.abee.gestao.Entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;


@Entity
@Data
public class Pedido {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int cdPedido;
    private int cdCliente;
    @Column(nullable = true)
    private Timestamp dataPedido;
    @Column(nullable = true)
    private String status;
    @Column(nullable = true)
    private String nomeCliente;
    @Column(nullable = true)
    private String cpfCnpjCliente;
    @Column(nullable = true)
    private String email;
    @Column(nullable = true)
    private String observacao;
    @Column(nullable = true)
    private String desconto;
    @Column(nullable = true)
    private int quantidadeParcela;
    @Column(nullable = true)
    private String formaPagamento;


    @Transient
    private List<ProdutoPedido> listaProduto;

    @Transient
    private String descricaoStatus;

    @Transient
    private BigDecimal valorPago;
}
