package br.com.abee.gestao.Rest;

import br.com.abee.gestao.Entity.Despesa;
import br.com.abee.gestao.Model.*;
import br.com.abee.gestao.Service.DespesaService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
@Slf4j
public class DespesaApi {
    @Autowired
    DespesaService service;

    @PostMapping("getDespesa")
    @CrossOrigin(origins = "*")
    public ResponseDespesa getDespesa(@RequestBody FiltroDespesa filtroDespesa) {
        try{
            return service.getDespesa(filtroDespesa);
        }catch(Exception e){
            log.error("Motivo do Erro: "+e.getMessage());
            e.printStackTrace();
            ResponseDespesa responseDespesa = new ResponseDespesa();
            responseDespesa.setListaDespesa(new ArrayList<>());
            responseDespesa.setQuantidadePagina(0);
            return responseDespesa;
        }
    }

    @DeleteMapping("/deleteDespesa/{id}")
    @CrossOrigin(origins = "*")
    public ResponseEntity<RespostaServico> deleteDespesa(@PathVariable("id") int id){
        RespostaServico respostaServico = new RespostaServico();
        try{
            service.delete(id);
            respostaServico.setCodigoServico(0);
            respostaServico.setMensagem("Sucesso ao apagar a despesa");
            return new ResponseEntity<RespostaServico>(respostaServico, HttpStatus.OK);
        }catch(Exception e){
            respostaServico.setCodigoServico(1);
            respostaServico.setMensagem("Erro ao tentar apagar a despesa");
            return new ResponseEntity<RespostaServico>(respostaServico, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/cadastroDespesa")
    @CrossOrigin(origins = "*")
    public ResponseEntity<RespostaServico> cadastroDespesa(@RequestBody Despesa despesa){
        RespostaServico respostaServico = new RespostaServico();
        try{
            service.cadastroDespesa(despesa);
            respostaServico.setCodigoServico(0);
            respostaServico.setMensagem("Despesa Cadastrada com sucesso");
            return new ResponseEntity<RespostaServico>(respostaServico, HttpStatus.OK);
        }catch(Exception e){
            log.error("Motivo do Erro: "+e.getMessage());
            e.printStackTrace();
            respostaServico.setCodigoServico(1);
            respostaServico.setMensagem("Erro ao tentar cadastrar a despesa, motivo:"+e.getMessage());
            return new ResponseEntity<RespostaServico>(respostaServico, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/atualizaDespesa")
    @CrossOrigin(origins = "*")
    public ResponseEntity<RespostaServico> atualizaDespesa(@RequestBody Despesa despesa){
        RespostaServico respostaServico = new RespostaServico();
        try{
            service.atualizaDespesa(despesa);
            respostaServico.setCodigoServico(0);
            respostaServico.setMensagem("Despesa Atualizada com sucesso");
            return  new ResponseEntity<RespostaServico>(respostaServico, HttpStatus.OK);
        }catch (Exception e){
            log.error("Motivo do Erro: " + e.getMessage());
            e.printStackTrace();
            respostaServico.setCodigoServico(1);
            respostaServico.setMensagem("Erro ao tentar atualizar a despesa, motivo: " + e.getMessage());
            return  new ResponseEntity<RespostaServico>(respostaServico, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
