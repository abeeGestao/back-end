package br.com.abee.gestao.Repository;

import br.com.abee.gestao.Model.Grafico;
import br.com.abee.gestao.Model.GraficoDespesa;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Component
public class GraficoDespesaMapper implements RowMapper<List<GraficoDespesa>> {
    @Override
    public List<GraficoDespesa> mapRow(ResultSet rs, int i) throws SQLException {
        List<GraficoDespesa> listaGrafico = new ArrayList<>();
        GraficoDespesa grafico = new GraficoDespesa();
        grafico.setTotal(rs.getInt("total"));
        grafico.setHoje(rs.getInt("hoje"));
        grafico.setUm(rs.getInt("um"));
        grafico.setDois(rs.getInt("dois"));
        grafico.setTres(rs.getInt("tres"));
        grafico.setQuatro(rs.getInt("quatro"));
        grafico.setCinco(rs.getInt("cinco"));
        grafico.setSeis(rs.getInt("seis"));
        listaGrafico.add(grafico);

        while (rs.next()){
            grafico = new GraficoDespesa();
            grafico.setTotal(rs.getInt("total"));
            grafico.setHoje(rs.getInt("hoje"));
            grafico.setUm(rs.getInt("um"));
            grafico.setDois(rs.getInt("dois"));
            grafico.setTres(rs.getInt("tres"));
            grafico.setQuatro(rs.getInt("quatro"));
            grafico.setCinco(rs.getInt("cinco"));
            grafico.setSeis(rs.getInt("seis"));
            listaGrafico.add(grafico);
        }

        return listaGrafico;
    }
}
