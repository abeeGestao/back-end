package br.com.abee.gestao.Repository;

import br.com.abee.gestao.Entity.Produto;
import br.com.abee.gestao.Model.Compra;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Component
public class CompraMapper implements RowMapper<List<Compra>> {
    @Override
    public List<Compra> mapRow(ResultSet rs, int i) throws SQLException {
        List<Compra> listaCompras = new ArrayList<>();
        Compra compra = new Compra();
        compra.setCodigo(rs.getInt("codigo"));
        compra.setNome(rs.getString("nome"));
        compra.setRazaoSocial(rs.getString("razaoSocial"));
        compra.setTotal(rs.getFloat("total"));

        listaCompras.add(compra);

        while (rs.next()){
            compra = new Compra();
            compra.setCodigo(rs.getInt("codigo"));
            compra.setNome(rs.getString("nome"));
            compra.setRazaoSocial(rs.getString("razaoSocial"));
            compra.setTotal(rs.getFloat("total"));

            listaCompras.add(compra);
        }

        return listaCompras;
    }
}
