package br.com.abee.gestao.Service;

import br.com.abee.gestao.Entity.Pedido;
import br.com.abee.gestao.Entity.ProdutoPedido;
import br.com.abee.gestao.Model.FiltrosCliente;
import br.com.abee.gestao.Model.FiltrosPedido;
import br.com.abee.gestao.Model.ResponseCliente;
import br.com.abee.gestao.Model.ResponseListaPedido;
import br.com.abee.gestao.Repository.ClienteRepository;
import br.com.abee.gestao.Repository.PedidoRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class PedidoService {

    private PedidoRepository repository;
    private ClienteRepository clienteRepository;

    @Autowired
    public PedidoService(PedidoRepository repository, ClienteRepository clienteRepository){
        this.repository = repository;
        this.clienteRepository = clienteRepository;
    }

    public ResponseListaPedido getPedidos(FiltrosPedido filtrosPedido) {
        return repository.getPedidos(filtrosPedido);
    }

    public void delete(int cdPedido) {
        repository.delete(cdPedido);
    }

    public void cadastroPedido(Pedido pedido) throws Exception {
        //calculando pedido
        for(ProdutoPedido pe : pedido.getListaProduto()){
            pe.setValorTotal(pe.getValor() * pe.getQuantidade());
        }

        //Buscando Nome do Cliente
        FiltrosCliente filtro = new FiltrosCliente();
        filtro.setCdCliente(pedido.getCdCliente());
        filtro.setPagina(1);
        filtro.setQuantidadePagina(10);
        try{
            ResponseCliente responseCliente = clienteRepository.getClientes(filtro);
            pedido.setNomeCliente(responseCliente.getListaCliente().get(0).getNome());
            pedido.setCpfCnpjCliente(responseCliente.getListaCliente().get(0).getDocumento());
            pedido.setEmail(responseCliente.getListaCliente().get(0).getEmail());
            pedido.setStatus("A");
        }catch(Exception e){
            e.printStackTrace();
            log.error("Erro ao tentar buscar o cliente no cadastro do pedido: "+e.getMessage() );
            throw new Exception("Erro ao tentar cadastrar um pedido com o cliente de código: "+filtro.getCdCliente());
        }


        //Cadastrando Pedido
        repository.cadastroPedido(pedido);
    }

    public void confirmarPedido(Pedido pedido) {
        repository.confirmarPedido(pedido);
    }
}
