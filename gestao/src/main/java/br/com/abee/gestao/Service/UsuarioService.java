package br.com.abee.gestao.Service;

import br.com.abee.gestao.Entity.Usuario;
import br.com.abee.gestao.Model.FiltrosUsuario;
import br.com.abee.gestao.Model.ResponseUsuario;
import br.com.abee.gestao.Repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UsuarioService {
    @Autowired
    UsuarioRepository repository;

    public ResponseUsuario getUsuarios(FiltrosUsuario filtrosUsuario) {
        return repository.getUsuarios(filtrosUsuario);
    }

    public void delete(int id) { repository.delete(id); }

    public void cadastroUsuario(Usuario usuario) {
        repository.cadastroUsuario(usuario);
    }

    public void atualizaUsuario(Usuario usuario) {
        repository.atualizaUsuario(usuario);
    }

}
