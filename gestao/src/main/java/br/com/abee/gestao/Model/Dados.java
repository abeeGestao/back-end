package br.com.abee.gestao.Model;

import lombok.Data;

@Data
public class Dados {
    private int totalProduto;
    private int totalCliente;
    private int totalPedido;
    private float totalValorVendido;
}
