package br.com.abee.gestao.Repository;

import br.com.abee.gestao.Entity.Despesa;
import br.com.abee.gestao.Entity.Produto;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Component
public class DespesaMapper implements RowMapper<List<Despesa>> {

    @Override
    public List<Despesa> mapRow(ResultSet rs, int rowNum) throws SQLException {
        List<Despesa> listaDespesa = new ArrayList<>();

        Despesa despesa = new Despesa();
        despesa.setId(rs.getInt("id"));
        despesa.setDescricao(rs.getString("descricao"));
        despesa.setData(rs.getTimestamp("data"));
        despesa.setValor(rs.getDouble("valor"));
        listaDespesa.add(despesa);
        while (rs.next()) {
            despesa = new Despesa();
            despesa.setId(rs.getInt("id"));
            despesa.setDescricao(rs.getString("descricao"));
            despesa.setData(rs.getTimestamp("data"));
            despesa.setValor(rs.getDouble("valor"));
            listaDespesa.add(despesa);
        }
        return listaDespesa;
    }

}
