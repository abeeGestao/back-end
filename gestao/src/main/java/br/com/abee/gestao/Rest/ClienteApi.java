package br.com.abee.gestao.Rest;

import br.com.abee.gestao.Entity.Cliente;
import br.com.abee.gestao.Model.ResponseCliente;
import br.com.abee.gestao.Model.FiltrosCliente;
import br.com.abee.gestao.Model.RespostaServico;
import br.com.abee.gestao.Service.ClienteService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;


@RestController
@Slf4j
public class ClienteApi {

    @Autowired
    ClienteService service;

    @PostMapping("/getClientes")
    @CrossOrigin(origins = "*")
    public ResponseCliente getClientes(@RequestBody FiltrosCliente filtrosCliente){

        try{
            return service.getClientes(filtrosCliente);
        }catch(Exception e){
            log.error("Motivo do Erro: "+e.getMessage());
            e.printStackTrace();
            ResponseCliente responseCliente = new ResponseCliente();
            responseCliente.setListaCliente(new ArrayList<>());
            responseCliente.setQuantidadePagina(0);
            return responseCliente;
        }
    }

    @GetMapping("/getAllClientes")
    @CrossOrigin(origins = "*")
    public ResponseCliente getClientes(){
        try{
            return service.getAllClientes();
        }catch(Exception e){
            log.error("Motivo do Erro: "+e.getMessage());
            e.printStackTrace();
            ResponseCliente responseCliente = new ResponseCliente();
            responseCliente.setListaCliente(new ArrayList<>());
            responseCliente.setQuantidadePagina(0);
            return responseCliente;
        }
    }

    @DeleteMapping("/deleteCliente/{id}")
    @CrossOrigin(origins = "*")
    public ResponseEntity<RespostaServico> deleteClientes(@PathVariable("id") int cdCliente){
        RespostaServico respostaServico = new RespostaServico();
        try{
            service.delete(cdCliente);
            respostaServico.setCodigoServico(0);
            respostaServico.setMensagem("Sucesso ao apagar o cliente");
            return new ResponseEntity<RespostaServico>(respostaServico, HttpStatus.OK);
        }catch(Exception e){
            log.error("Motivo do Erro: "+e.getMessage());
            e.printStackTrace();
            respostaServico.setCodigoServico(1);
            respostaServico.setMensagem("Erro ao tentar apagar o cliente");
            return new ResponseEntity<RespostaServico>(respostaServico, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @PostMapping("/cadastroCliente")
    @CrossOrigin(origins = "*")
    public ResponseEntity<RespostaServico> cadastroCliente(@RequestBody Cliente cliente){
        RespostaServico respostaServico = new RespostaServico();
        try{
            service.cadastroCliente(cliente);
            respostaServico.setCodigoServico(0);
            respostaServico.setMensagem("Cliente Cadastrado com sucesso");
            return new ResponseEntity<RespostaServico>(respostaServico, HttpStatus.OK);
        }catch(Exception e){
            log.error("Motivo do Erro: "+e.getMessage());
            e.printStackTrace();
            respostaServico.setCodigoServico(1);
            respostaServico.setMensagem("Erro ao tentar cadastrar o cliente, motivo:"+e.getMessage());
            return new ResponseEntity<RespostaServico>(respostaServico, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/atualizarCliente")
    @CrossOrigin(origins = "*")
    public ResponseEntity<RespostaServico> atualizarCliente(@RequestBody Cliente cliente){
        RespostaServico respostaServico = new RespostaServico();
        try{
            service.atualizarCliente(cliente);
            respostaServico.setCodigoServico(0);
            respostaServico.setMensagem("Cliente Atualizado com sucesso");
            return new ResponseEntity<RespostaServico>(respostaServico, HttpStatus.OK);
        }catch(Exception e){
            log.error("Motivo do Erro: "+e.getMessage());
            e.printStackTrace();
            respostaServico.setCodigoServico(1);
            respostaServico.setMensagem("Erro ao tentar atualizar o cliente, motivo: "+e.getMessage());
            return new ResponseEntity<RespostaServico>(respostaServico, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}