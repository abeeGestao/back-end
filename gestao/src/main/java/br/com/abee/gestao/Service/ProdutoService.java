package br.com.abee.gestao.Service;

import br.com.abee.gestao.Entity.Cliente;
import br.com.abee.gestao.Entity.Produto;
import br.com.abee.gestao.Model.FiltrosProduto;
import br.com.abee.gestao.Model.ResponseProduto;
import br.com.abee.gestao.Repository.ProdutoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProdutoService {
    @Autowired
    ProdutoRepository repository;

    public ResponseProduto getProdutos(FiltrosProduto filtrosProduto) {
        return repository.getProdutos(filtrosProduto);
    }

    public void delete(int id) { repository.delete(id); }

    public void cadastroProduto(Produto produto) {
        repository.cadastroProduto(produto);
    }

    public void atualizaProduto(Produto produto) { repository.atualizaProduto(produto); }

    public ResponseProduto getAllProdutos() {
        return repository.getAllProdutos();
    }
}
