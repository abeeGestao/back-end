package br.com.abee.gestao.Service;

import br.com.abee.gestao.Entity.Cliente;
import br.com.abee.gestao.Model.ResponseCliente;
import br.com.abee.gestao.Model.FiltrosCliente;
import br.com.abee.gestao.Repository.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ClienteService {
    @Autowired
    ClienteRepository repository;

    public ResponseCliente getClientes(FiltrosCliente filtrosCliente) {
        return repository.getClientes(filtrosCliente);
    }

    public void delete(int id) {
        repository.delete(id);
    }

    public void cadastroCliente(Cliente cliente) {
        repository.cadastroCliente(cliente);
    }

    public ResponseCliente getAllClientes() {
        return repository.getAllClientes();
    }

    public void atualizarCliente(Cliente cliente) throws Exception {
        if(cliente.getCdCliente() != 0 ){
            repository.atualizarCliente(cliente);
        }else {
            throw new Exception("Código do cliente está nulo!");
        }

    }
}
