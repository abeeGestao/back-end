package br.com.abee.gestao.Repository;

import br.com.abee.gestao.Entity.Cliente;
import br.com.abee.gestao.Model.ResponseCliente;
import br.com.abee.gestao.Model.FiltrosCliente;
import br.com.abee.gestao.Model.ResponseDashboard;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
@Repository
@Slf4j
public class ClienteRepository{

    NamedParameterJdbcTemplate jdbcTemplate;
    ClienteMapper clienteMapper;
    QuantidadePaginaMapper quantidadePaginaMapper;

    @Autowired
    public ClienteRepository(NamedParameterJdbcTemplate jdbcTemplate, ClienteMapper clienteMapper, QuantidadePaginaMapper quantidadePaginaMapper){
        this.jdbcTemplate = jdbcTemplate;
        this.clienteMapper = clienteMapper;
        this.quantidadePaginaMapper = quantidadePaginaMapper;
    }
    public ResponseCliente getClientes(FiltrosCliente filtrosCliente) {
        if(filtrosCliente.getPagina() == 0){filtrosCliente.setPagina(1);}
        if(filtrosCliente.getQuantidadePagina() == 0){filtrosCliente.setQuantidadePagina(1);}
        log.info("buscando Cliente, Filtros:", filtrosCliente);
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("cdCliente", filtrosCliente.getCdCliente())
                .addValue("nome", filtrosCliente.getNome())
                .addValue("documento", filtrosCliente.getDocumento())
                .addValue("razaoSocial", filtrosCliente.getRazaoSocial())
                .addValue("nomeFantasia", filtrosCliente.getNomeFantasia())
                .addValue("status", filtrosCliente.getStatus())
                .addValue("email", filtrosCliente.getEmail())
                .addValue("pagina", filtrosCliente.getPagina())
                .addValue("quantidadePagina", filtrosCliente.getQuantidadePagina());

        String sql = "select * from cliente where " +
                "(cdCliente = :cdCliente or :cdCliente is null or 0 = :cdCliente )"+
                " and (nome like :nome or :nome IS null or '' = :nome )" +
                " and (documento = :documento  or :documento IS null or '' = :documento )" +
                " and (razaoSocial = :razaoSocial  or :razaoSocial IS null or '' = :razaoSocial )" +
                " and (nomeFantasia = :nomeFantasia  or :nomeFantasia IS null or '' = :nomeFantasia )" +
                " and (status = :status  or :status IS null or '' = :status )" +
                " and (email = :email or :email IS null or '' = :email )" +
                " order by cdcliente DESC " +
                " offset (:pagina - 1) * :quantidadePagina rows fetch next :quantidadePagina rows only;";
        List<Cliente> listaCliente;
        try{
            listaCliente = jdbcTemplate.queryForObject(sql, params, clienteMapper);
        }catch(EmptyResultDataAccessException e){
            listaCliente = new ArrayList<>();
        }
        ResponseCliente response = new ResponseCliente();
        response.setListaCliente(listaCliente);
        response.setQuantidadePagina(getQuantidadeRegistro(params));
        return response;
    }

    private int getQuantidadeRegistro(MapSqlParameterSource params){
        String sql = "select count(*) as total from cliente where " +
                "(cdCliente = :cdCliente or :cdCliente is null or 0 = :cdCliente )"+
                " and (nome = :nome or :nome IS null or '' = :nome )" +
                " and (documento = :documento  or :documento IS null or '' = :documento )" +
                " and (razaoSocial = :razaoSocial  or :razaoSocial IS null or '' = :razaoSocial )" +
                " and (nomeFantasia = :nomeFantasia  or :nomeFantasia IS null or '' = :nomeFantasia )" +
                " and (status = :status  or :status IS null or '' = :status )" +
                " and (email = :email or :email IS null or '' = :email )";

        return jdbcTemplate.queryForObject(sql, params,quantidadePaginaMapper);
    }

    public void delete(int cdCliente) {
        log.info("Apagando cliente");
        String sql = "delete from cliente " +
                " where :cdCliente = cdCliente ";
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("cdCliente", cdCliente);
        jdbcTemplate.update(sql, params);
        log.info("Cliente Apagado");
    }

    public void cadastroCliente(Cliente cliente) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("nome", cliente.getNome())
                .addValue("documento", cliente.getDocumento())
                .addValue("razaoSocial", cliente.getRazaoSocial())
                .addValue("nomeFantasia", cliente.getNomeFantasia())
                .addValue("status", cliente.getStatus())
                .addValue("email", cliente.getEmail())
                .addValue("cep", cliente.getCep())
                .addValue("estado", cliente.getEstado())
                .addValue("pais", cliente.getPais())
                .addValue("rua", cliente.getRua())
                .addValue("bairro", cliente.getBairro())
                .addValue("numero", cliente.getNumero())
                .addValue("telefone", cliente.getTelefone())
                .addValue("cidade", cliente.getCidade());
        String sql = "insert into cliente\n" +
                " (cep , cidade , documento ,email , estado, numero , nome , nomeFantasia ,pais , bairro ,razaoSocial ,rua ,status ,telefone ) " +
                "values" +
                "(:cep , :cidade , :documento , :email , :estado , :numero , :nome , :nomeFantasia , :pais, :bairro, :razaoSocial , :rua , :status , :telefone );";
        jdbcTemplate.update(sql, params);
    }

    public ResponseCliente getAllClientes() {
        MapSqlParameterSource params = new MapSqlParameterSource();
        String sql = "select * from cliente";
        List<Cliente> listaCliente = jdbcTemplate.queryForObject(sql, params, clienteMapper);
        ResponseCliente response = new ResponseCliente();
        response.setListaCliente(listaCliente);
        return response;
    }

    public void atualizarCliente(Cliente cliente) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("cdCliente", cliente.getCdCliente())
                .addValue("nome", cliente.getNome())
                .addValue("documento", cliente.getDocumento())
                .addValue("razaoSocial", cliente.getRazaoSocial())
                .addValue("nomeFantasia", cliente.getNomeFantasia())
                .addValue("status", cliente.getStatus())
                .addValue("email", cliente.getEmail())
                .addValue("cep", cliente.getCep())
                .addValue("estado", cliente.getEstado())
                .addValue("pais", cliente.getPais())
                .addValue("rua", cliente.getRua())
                .addValue("bairro", cliente.getBairro())
                .addValue("numero", cliente.getNumero())
                .addValue("telefone", cliente.getTelefone())
                .addValue("cidade", cliente.getCidade());
        String sql = " update cliente " +
                " set " +
                " cep = :cep , cidade = :cidade , documento = :documento , email = :email , estado = :estado , " +
                " numero = :numero   , nome = :nome   , nomeFantasia = :nomeFantasia , pais = :pais , " +
                " bairro = :bairro   ,razaoSocial = :razaoSocial , rua = :rua , status = :status , telefone = :telefone " +
                " where cdCliente = :cdCliente";
        jdbcTemplate.update(sql, params);
    }

}
