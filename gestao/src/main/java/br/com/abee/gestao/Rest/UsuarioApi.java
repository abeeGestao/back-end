package br.com.abee.gestao.Rest;

import br.com.abee.gestao.Entity.Produto;
import br.com.abee.gestao.Entity.Usuario;
import br.com.abee.gestao.Model.FiltrosUsuario;
import br.com.abee.gestao.Model.ResponseUsuario;
import br.com.abee.gestao.Model.RespostaServico;
import br.com.abee.gestao.Service.UsuarioService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
@Slf4j
public class UsuarioApi {
    @Autowired
    UsuarioService service;

    @PostMapping("getUsuarios")
    @CrossOrigin(origins = "*")
    public ResponseUsuario getUsuarios(@RequestBody FiltrosUsuario filtrosUsuario) {
        try{
            return service.getUsuarios(filtrosUsuario);
        }catch(Exception e){
            log.error("Motivo do Erro: "+e.getMessage());
            e.printStackTrace();
            ResponseUsuario responseUsuario = new ResponseUsuario();
            responseUsuario.setListaUsuario(new ArrayList<>());
            responseUsuario.setQuantidadePagina(0);
            return responseUsuario;
        }
    }

    @DeleteMapping("/deleteUsuario/{id}")
    @CrossOrigin(origins = "*")
    public ResponseEntity<RespostaServico> deleteUsuarios(@PathVariable("id") int id){
        RespostaServico respostaServico = new RespostaServico();
        try{
            service.delete(id);
            respostaServico.setCodigoServico(0);
            respostaServico.setMensagem("Sucesso ao apagar o usuario");
            return new ResponseEntity<RespostaServico>(respostaServico, HttpStatus.OK);
        }catch(Exception e){
            respostaServico.setCodigoServico(1);
            respostaServico.setMensagem("Erro ao tentar apagar o usuario");
            return new ResponseEntity<RespostaServico>(respostaServico, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/cadastroUsuario")
    @CrossOrigin(origins = "*")
    public ResponseEntity<RespostaServico> cadastroUsuario(@RequestBody Usuario usuario){
        RespostaServico respostaServico = new RespostaServico();
        try{
            service.cadastroUsuario(usuario);
            respostaServico.setCodigoServico(0);
            respostaServico.setMensagem("Usuario Cadastrado com sucesso");
            return new ResponseEntity<RespostaServico>(respostaServico, HttpStatus.OK);
        }catch(Exception e){
            log.error("Motivo do Erro: "+e.getMessage());
            e.printStackTrace();
            respostaServico.setCodigoServico(1);
            respostaServico.setMensagem("Erro ao tentar cadastrar o usuario, motivo:"+e.getMessage());
            return new ResponseEntity<RespostaServico>(respostaServico, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/atualizaUsuario")
    @CrossOrigin(origins = "*")
    public ResponseEntity<RespostaServico> atualizaUsuario(@RequestBody Usuario usuario){
        RespostaServico respostaServico = new RespostaServico();
        try{
            service.atualizaUsuario(usuario);
            respostaServico.setCodigoServico(0);
            respostaServico.setMensagem("Usuário Atualizado com sucesso");
            return new ResponseEntity<RespostaServico>(respostaServico, HttpStatus.OK);
        }catch(Exception e){
            log.error("Motivo do Erro: "+e.getMessage());
            e.printStackTrace();
            respostaServico.setCodigoServico(1);
            respostaServico.setMensagem("Erro ao tentar atualizar o usuario, motivo:"+e.getMessage());
            return new ResponseEntity<RespostaServico>(respostaServico, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
