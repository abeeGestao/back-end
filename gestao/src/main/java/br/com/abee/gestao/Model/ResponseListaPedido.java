package br.com.abee.gestao.Model;

import br.com.abee.gestao.Entity.Pedido;
import lombok.Data;

import java.util.List;

@Data
public class ResponseListaPedido {
    private int quantidadePagina;
    private List<Pedido> listaPedido;
}
