package br.com.abee.gestao.Repository;

import br.com.abee.gestao.Entity.Pedido;
import br.com.abee.gestao.Model.Compra;
import br.com.abee.gestao.Model.ItensPedido;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Component
public class ItensMapper implements RowMapper<List<ItensPedido>> {
    @Override
    public List<ItensPedido> mapRow(ResultSet rs, int i) throws SQLException {
        List<ItensPedido> listaItens = new ArrayList<>();
        ItensPedido item = new ItensPedido();
        item.setCdPedido(rs.getInt("cdPedido"));
        item.setCodigo(rs.getInt("codigo"));
        item.setNome(rs.getString("nome"));
        item.setQuantidade(rs.getInt("quantidade"));
        item.setValor(rs.getFloat("valor"));
        item.setTotal(rs.getFloat("total"));
        item.setObservacao(rs.getString("observacao"));
        item.setCliente(rs.getString("cliente"));
        item.setPagamento(rs.getString("pagamento"));
        item.setData(rs.getTimestamp("data"));
        item.setStatus(rs.getString("status"));

        listaItens.add(item);

        while (rs.next()){
            item = new ItensPedido();
            item.setCdPedido(rs.getInt("cdPedido"));
            item.setCodigo(rs.getInt("codigo"));
            item.setNome(rs.getString("nome"));
            item.setQuantidade(rs.getInt("quantidade"));
            item.setValor(rs.getFloat("valor"));
            item.setTotal(rs.getFloat("total"));
            item.setObservacao(rs.getString("observacao"));
            item.setCliente(rs.getString("cliente"));
            item.setPagamento(rs.getString("pagamento"));
            item.setData(rs.getTimestamp("data"));
            item.setStatus(rs.getString("status"));

            listaItens.add(item);
        }

        return listaItens;
    }
}
