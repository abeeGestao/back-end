package br.com.abee.gestao.Rest;

import br.com.abee.gestao.Model.FiltroItensPedido;
import br.com.abee.gestao.Model.ResponseItensPedido;
import br.com.abee.gestao.Service.ItensPedidoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

@RestController
@Slf4j
public class ItensPedidoApi {

    @Autowired
    ItensPedidoService service;

    @PostMapping("getItensPedido")
    @CrossOrigin(origins = "*")
    public ResponseItensPedido getListaItens(@RequestBody FiltroItensPedido filtroItensPedido){

        try{
            return service.getListaItens(filtroItensPedido);
        }catch (Exception e){
            e.printStackTrace();
            log.error("Erro ao buscar pedidos: "+e.getMessage());
            ResponseItensPedido itensResponse = new ResponseItensPedido();
            itensResponse.getListaItens(new ArrayList<>());
            itensResponse.setQuantidadePagina(0);
            return itensResponse;
        }
    }
}
