package br.com.abee.gestao.Model;

import br.com.abee.gestao.Entity.Usuario;
import lombok.Data;

import java.util.List;

@Data
public class ResponseUsuario {
    private int quantidadePagina;
    private List<Usuario> listaUsuario;
}
