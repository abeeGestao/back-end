package br.com.abee.gestao.Model;

import lombok.Data;

@Data
public class FiltrosPedido {
    private int cdPedido;
    private int cdCliente;
    private String status;
    private int pagina;
    private int quantidadePagina;
    private String cpfCnpjCliente;
    private String email;

}
