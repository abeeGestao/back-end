package br.com.abee.gestao.Model;

import lombok.Data;

@Data
public class FiltrosUsuario {
    private int id;
    private String nome;
    private String nomeUsuario;
    private int perfil;
    private String senha;
    private int pagina;
    private int quantidadePagina;

}
