package br.com.abee.gestao.Model;

import br.com.abee.gestao.Entity.Cliente;
import lombok.Data;

import java.util.List;

@Data
public class ResponseCliente {
    private int quantidadePagina;
    private List<Cliente> listaCliente;
}
