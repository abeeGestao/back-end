package br.com.abee.gestao.Repository;

import br.com.abee.gestao.Entity.Pedido;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class PedidoMapper  implements RowMapper<List<Pedido>> {
    public List<Pedido> mapRow(ResultSet rs, int rowNum) throws SQLException {
        List<Pedido> listaPedido = new ArrayList<>();
        Pedido pedido = new Pedido();
        pedido.setCdCliente(rs.getInt("cdCliente"));
        pedido.setCdPedido(rs.getInt("cdPedido"));
        pedido.setStatus(Optional.ofNullable(rs.getString("status")).orElse(""));
        pedido.setNomeCliente(Optional.ofNullable(rs.getString("nomeCliente")).orElse(""));
        pedido.setDataPedido(rs.getTimestamp("dataPedido"));
        pedido.setDesconto(Optional.ofNullable(rs.getString("desconto")).orElse(""));
        pedido.setFormaPagamento(Optional.ofNullable(rs.getString("formaPagamento")).orElse(""));
        pedido.setObservacao(Optional.ofNullable(rs.getString("observacao")).orElse(""));
        pedido.setQuantidadeParcela(rs.getInt("quantidadeParcela"));
        pedido.setNomeCliente(Optional.ofNullable(rs.getString("nome")).orElse(""));
        if(pedido.getStatus().equals("A")){
            pedido.setDescricaoStatus("Aberto");
        }else if(pedido.getStatus().equals("C")){
            pedido.setDescricaoStatus("Confirmado");
        }
        listaPedido.add(pedido);
        while(rs.next()){
            pedido = new Pedido();
            pedido.setCdCliente(rs.getInt("cdCliente"));
            pedido.setCdPedido(rs.getInt("cdPedido"));
            pedido.setStatus(Optional.ofNullable(rs.getString("status")).orElse(""));
            pedido.setNomeCliente(Optional.ofNullable(rs.getString("nomeCliente")).orElse(""));
            pedido.setDataPedido(rs.getTimestamp("dataPedido"));
            pedido.setDesconto(Optional.ofNullable(rs.getString("desconto")).orElse(""));
            pedido.setFormaPagamento(Optional.ofNullable(rs.getString("formaPagamento")).orElse(""));
            pedido.setObservacao(Optional.ofNullable(rs.getString("observacao")).orElse(""));
            pedido.setQuantidadeParcela(rs.getInt("quantidadeParcela"));
            pedido.setNomeCliente(Optional.ofNullable(rs.getString("nome")).orElse(""));
            if(pedido.getStatus().equals("A")){
                pedido.setDescricaoStatus("Aberto");
            }else if(pedido.getStatus().equals("C")){
                pedido.setDescricaoStatus("Confirmado");
            }
            listaPedido.add(pedido);
        }
        return listaPedido;
    }
}
