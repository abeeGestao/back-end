package br.com.abee.gestao.Model;

import br.com.abee.gestao.Entity.Cliente;
import lombok.Data;

import java.sql.Timestamp;

@Data
public class ItensPedido {
    private int cdPedido;
    private int codigo;
    private String nome;
    private float valor;
    private int quantidade;
    private float total;
    private String observacao;
    private String cliente;
    private String pagamento;
    private Timestamp data;
    private Cliente clienteObjeto;
    private String status;

}
