package br.com.abee.gestao.Model;

import lombok.Data;

@Data
public class FiltrosCliente {
    private int cdCliente;
    private String nome;
    private String documento;
    private String razaoSocial;
    private String nomeFantasia;
    private String status;
    private String email;
    private int pagina;
    private int quantidadePagina;


}
