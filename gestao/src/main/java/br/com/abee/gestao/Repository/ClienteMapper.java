package br.com.abee.gestao.Repository;

import br.com.abee.gestao.Entity.Cliente;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class ClienteMapper implements RowMapper<List<Cliente>> {
    @Override
    public List<Cliente> mapRow(ResultSet rs, int rowNum) throws SQLException {
        List<Cliente> listaCliente = new ArrayList<>();
        Cliente cliente = new Cliente();
        cliente.setCdCliente(rs.getInt("cdCliente"));
        cliente.setCep(Optional.ofNullable(rs.getString("cep")).orElse(""));
        cliente.setCidade(Optional.ofNullable(rs.getString("cidade")).orElse(""));
        cliente.setDocumento(Optional.ofNullable(rs.getString("documento")).orElse(""));
        cliente.setEmail(Optional.ofNullable(rs.getString("email")).orElse(""));
        cliente.setEstado(Optional.ofNullable(rs.getString("estado")).orElse(""));
        cliente.setNome(Optional.ofNullable(rs.getString("nome")).orElse(""));
        cliente.setNomeFantasia(Optional.ofNullable(rs.getString("nomeFantasia")).orElse(""));
        cliente.setNumero(Optional.ofNullable(rs.getString("numero")).orElse(""));
        cliente.setPais(Optional.ofNullable(rs.getString("pais")).orElse(""));
        cliente.setRazaoSocial(Optional.ofNullable(rs.getString("razaoSocial")).orElse(""));
        cliente.setRua(Optional.ofNullable(rs.getString("rua")).orElse(""));
        cliente.setBairro(Optional.ofNullable(rs.getString("bairro")).orElse(""));
        cliente.setStatus(Optional.ofNullable(rs.getString("status")).orElse(""));
        cliente.setTelefone(Optional.ofNullable(rs.getString("telefone")).orElse(""));
        listaCliente.add(cliente);
        while(rs.next()) {
            cliente = new Cliente();
            cliente.setCdCliente(rs.getInt("cdCliente"));
            cliente.setCep(Optional.ofNullable(rs.getString("cep")).orElse(""));
            cliente.setCidade(Optional.ofNullable(rs.getString("cidade")).orElse(""));
            cliente.setDocumento(Optional.ofNullable(rs.getString("documento")).orElse(""));
            cliente.setEmail(Optional.ofNullable(rs.getString("email")).orElse(""));
            cliente.setEstado(Optional.ofNullable(rs.getString("estado")).orElse(""));
            cliente.setNome(Optional.ofNullable(rs.getString("nome")).orElse(""));
            cliente.setNomeFantasia(Optional.ofNullable(rs.getString("nomeFantasia")).orElse(""));
            cliente.setNumero(Optional.ofNullable(rs.getString("numero")).orElse(""));
            cliente.setPais(Optional.ofNullable(rs.getString("pais")).orElse(""));
            cliente.setRazaoSocial(Optional.ofNullable(rs.getString("razaoSocial")).orElse(""));
            cliente.setRua(Optional.ofNullable(rs.getString("rua")).orElse(""));
            cliente.setBairro(Optional.ofNullable(rs.getString("bairro")).orElse(""));
            cliente.setStatus(Optional.ofNullable(rs.getString("status")).orElse(""));
            cliente.setTelefone(Optional.ofNullable(rs.getString("telefone")).orElse(""));
            listaCliente.add(cliente);
        }
        return listaCliente;
    }
}
