package br.com.abee.gestao.Repository;

import br.com.abee.gestao.Model.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Slf4j
public class GraficoRepository {

    NamedParameterJdbcTemplate jdbcTemplate;
    GraficoMapper graficoMapper;

    @Autowired
    public GraficoRepository(NamedParameterJdbcTemplate jdbcTemplate, GraficoMapper graficoMapper){
        this.jdbcTemplate = jdbcTemplate;
        this.graficoMapper = graficoMapper;
    }

    public ResponseGrafico devolveDados(FiltroGrafico filtroGrafico){

        log.info("buscando Dados, Filtros:", filtroGrafico);
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("total", filtroGrafico.getTotal())
                .addValue("hoje", filtroGrafico.getHoje())
                .addValue("um", filtroGrafico.getUm())
                .addValue("dois", filtroGrafico.getDois())
                .addValue("tres", filtroGrafico.getTres())
                .addValue("quatro", filtroGrafico.getQuatro())
                .addValue("cinco", filtroGrafico.getCinco())
                .addValue("seis", filtroGrafico.getSeis());

        String sql = "SELECT \n" +
                "(select count(*) from pedido p where trunc(p.datapedido) between trunc(sysdate) - 5 and trunc(sysdate) ) total,\n" +
                "(select count(*) from pedido p where trunc(p.datapedido) = trunc(sysdate) )  hoje,\n" +
                "(select count(*) from pedido p where trunc(p.datapedido) = trunc(sysdate) - 1 )  um,\n" +
                "(select count(*) from pedido p where trunc(p.datapedido) = trunc(sysdate) - 2 )  dois,\n" +
                "(select count(*) from pedido p where trunc(p.datapedido) = trunc(sysdate) - 3 )  tres,\n" +
                "(select count(*) from pedido p where trunc(p.datapedido) = trunc(sysdate) - 4 )  quatro,\n" +
                "(select count(*) from pedido p where trunc(p.datapedido) = trunc(sysdate) - 5 )  cinco,\n" +
                "(select count(*) from pedido p where trunc(p.datapedido) = trunc(sysdate) - 6 )  seis,\n" +
                "FROM dual";
        List<Grafico> listaGrafico = jdbcTemplate.queryForObject(sql, params, graficoMapper);
        ResponseGrafico response = new ResponseGrafico();
        response.setListaGrafico(listaGrafico);

        return response;
    }

}
