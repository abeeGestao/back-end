package br.com.abee.gestao.Model;

import lombok.Data;

import java.util.List;

@Data
public class ResponseGrafico {

    private List<Grafico> listaGrafico;

}
