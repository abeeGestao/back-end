package br.com.abee.gestao.Entity;

import lombok.Data;
import javax.persistence.*;

@Entity
@Data
public class ProdutoPedido {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private int cdProdutoPedido;
    private int cdPedido;
    private int cdProduto;
    @Column(nullable = true)
    private int quantidade;
    private Double valor;
    private Double valorTotal;
    @Column(nullable = true)
    private String nome;
    @Column(nullable = true)
    private String descricao;
}
