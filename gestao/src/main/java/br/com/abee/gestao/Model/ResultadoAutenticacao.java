package br.com.abee.gestao.Model;

import lombok.Data;

@Data
public class ResultadoAutenticacao {
    private String usuario;
    private String nome;
    private boolean autenticado;
    private int perfil;

}
