package br.com.abee.gestao.Service;

import br.com.abee.gestao.Entity.Despesa;
import br.com.abee.gestao.Model.FiltroDespesa;
import br.com.abee.gestao.Model.ResponseDespesa;
import br.com.abee.gestao.Repository.DespesaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DespesaService {
    @Autowired
    DespesaRepository repository;

    public ResponseDespesa getDespesa(FiltroDespesa filtroDespesa) {
        return repository.getDespesa(filtroDespesa);
    }

    public void delete(int id) { repository.delete(id); }

    public void cadastroDespesa(Despesa despesa) {
        repository.cadastroDespesa(despesa);
    }

    public void atualizaDespesa(Despesa despesa) { repository.atualizaDespesa(despesa); }

}
