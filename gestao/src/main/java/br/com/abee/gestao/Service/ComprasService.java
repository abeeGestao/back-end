package br.com.abee.gestao.Service;

import br.com.abee.gestao.Model.FiltrosCompra;
import br.com.abee.gestao.Model.ResponseCompra;
import br.com.abee.gestao.Repository.ComprasRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ComprasService {

    @Autowired
    ComprasRepository repository;


    public ResponseCompra getDevolveCompra(FiltrosCompra filtrosCompra) {
        return repository.devolveCompra(filtrosCompra);
    }
}
