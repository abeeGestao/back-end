package br.com.abee.gestao.Rest;

import br.com.abee.gestao.Entity.Pedido;
import br.com.abee.gestao.Model.RespostaServico;
import br.com.abee.gestao.Service.CaixaService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
public class CaixaApi {
    CaixaService service;

    @Autowired
    public CaixaApi(CaixaService service){
        this.service = service;
    }

    @PostMapping("/cadastroPedidoPdv")
    @CrossOrigin(origins = "*")
    public ResponseEntity<RespostaServico> cadastroPedido(@RequestBody Pedido pedido){
        RespostaServico respostaServico = new RespostaServico();
        try{
            service.cadastrarPedidoPdv(pedido);
            respostaServico.setCodigoServico(0);
            respostaServico.setMensagem("Pedido Cadastrado com sucesso");
            return new ResponseEntity<RespostaServico>(respostaServico, HttpStatus.OK);
        }catch(Exception e){
            log.error("Motivo do Erro: "+e.getMessage());
            e.printStackTrace();
            respostaServico.setCodigoServico(1);
            respostaServico.setMensagem("Erro ao tentar cadastrar o Pedido, motivo:"+e.getMessage());
            return new ResponseEntity<RespostaServico>(respostaServico, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
