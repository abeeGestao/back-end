package br.com.abee.gestao.Repository;

import br.com.abee.gestao.Entity.Pedido;
import br.com.abee.gestao.Entity.ProdutoPedido;
import br.com.abee.gestao.Model.FiltrosPedido;
import br.com.abee.gestao.Model.ResponseListaPedido;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;


@Repository
@Slf4j
public class PedidoRepository {

    NamedParameterJdbcTemplate jdbcTemplate;
    PedidoMapper pedidoMapper;
    QuantidadePaginaMapper quantidadePaginaMapper;
    ProdutoPedidoRepository produtoPedidoRepository;

    @Autowired
    public PedidoRepository(NamedParameterJdbcTemplate jdbcTemplate, PedidoMapper pedidoMapper, QuantidadePaginaMapper quantidadePaginaMapper, ProdutoPedidoRepository produtoPedidoRepository){
        this.jdbcTemplate = jdbcTemplate;
        this.pedidoMapper = pedidoMapper;
        this.quantidadePaginaMapper = quantidadePaginaMapper;
        this.produtoPedidoRepository = produtoPedidoRepository;
    }
    public ResponseListaPedido getPedidos(FiltrosPedido filtrosPedido) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("cdCliente", filtrosPedido.getCdCliente())
                .addValue("cdPedido", filtrosPedido.getCdPedido())
                .addValue("status", filtrosPedido.getStatus())
                .addValue("email", filtrosPedido.getEmail())
                .addValue("pagina", filtrosPedido.getPagina())
                .addValue("quantidadePagina", filtrosPedido.getQuantidadePagina())
                .addValue("cpfCnpjCliente", filtrosPedido.getCpfCnpjCliente());

        String sql = "select * from pedido p" +
                " LEFT JOIN cliente as c on c.cdcliente =  p.cdcliente " +
                " where (c.cdCliente = :cdCliente or :cdCliente is null or 0 = :cdCliente ) " +
                " and (cdPedido = :cdPedido or :cdPedido is null or 0 = :cdPedido ) "+
                " and (cpfCnpjCliente = :cpfCnpjCliente or :cpfCnpjCliente is null or 0 = :cpfCnpjCliente ) "+
                " and (p.status = :status or :status is null or 'null' = :status ) "+
                " order by p.cdPedido DESC" +
                " offset (:pagina - 1) * :quantidadePagina rows fetch next :quantidadePagina rows only;";
        List<Pedido> listaPedido;
        try{
            listaPedido = jdbcTemplate.queryForObject(sql, params, pedidoMapper);
        }catch(EmptyResultDataAccessException e){
            listaPedido = new ArrayList<>();
        }
        ResponseListaPedido response = new ResponseListaPedido();
        response.setListaPedido(listaPedido);
        response.setQuantidadePagina(getQuantidadeRegistro(params));
        return response;
    }

    private int getQuantidadeRegistro(MapSqlParameterSource params){
        String sql = "select count(*) as total from pedido p" +
                " INNER JOIN cliente as c on c.cdcliente =  p.cdcliente " +
                " where (c.cdCliente = :cdCliente or :cdCliente is null or 0 = :cdCliente ) " +
                " and (cdPedido = :cdPedido or :cdPedido is null or 0 = :cdPedido ) "+
                " and (cpfCnpjCliente = :cpfCnpjCliente or :cpfCnpjCliente is null or 0 = :cpfCnpjCliente ) "+
                " and (p.status = :status or :status is null or 'null' = :status ) ";

        return jdbcTemplate.queryForObject(sql, params,quantidadePaginaMapper);
    }

    public void delete(int cdPedido) {
        log.info("Apagando pedido");
        String sql = "delete from pedido " +
                " where :cdPedido = cdPedido; " +
                " delete from  produtopedido " +
                " where :cdPedido = cdPedido; ";
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("cdPedido", cdPedido);
        jdbcTemplate.update(sql, params);
        log.info("Pedido Apagado");
    }

    @Transactional
    public void cadastroPedido(Pedido pedido) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("cdCliente", pedido.getCdCliente())
                .addValue("nomeCliente", pedido.getNomeCliente())
                .addValue("observacao", pedido.getObservacao())
                .addValue("desconto", pedido.getDesconto())
                .addValue("quantidadeParcela", pedido.getQuantidadeParcela())
                .addValue("formaPagamento", pedido.getFormaPagamento())
                .addValue("cpfCnpjCliente", pedido.getCpfCnpjCliente())
                .addValue("email", pedido.getEmail())
                .addValue("status",pedido.getStatus());
        String sql = "insert into pedido " +
                " (cdCliente ,nomeCliente, cpfCnpjCliente ,observacao ,desconto ,quantidadeParcela ,formaPagamento, dataPedido, status) " +
                " values" +
                " (:cdCliente ,:nomeCliente, :cpfCnpjCliente ,:observacao ,:desconto ,:quantidadeParcela ,:formaPagamento, now(), :status) ";

        //Cadastrando Pedido
        jdbcTemplate.update(sql, params, keyHolder);
        pedido.setCdPedido(keyHolder.getKey().intValue());
        for(ProdutoPedido produtoPedido : pedido.getListaProduto()){
            produtoPedido.setCdPedido(pedido.getCdPedido());
            produtoPedidoRepository.cadastroProdutoPedido(produtoPedido);
        }
    }


    public void confirmarPedido(Pedido pedido) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("cdPedido", pedido.getCdPedido());
        String sql = "update pedido set status = 'C' " +
                "where cdPedido = :cdPedido";
        jdbcTemplate.update(sql, params);
    }
}
