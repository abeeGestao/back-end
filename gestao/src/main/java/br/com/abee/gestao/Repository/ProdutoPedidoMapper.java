package br.com.abee.gestao.Repository;

import br.com.abee.gestao.Entity.ProdutoPedido;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Component
public class ProdutoPedidoMapper implements RowMapper<List<ProdutoPedido>> {
    public List<ProdutoPedido> mapRow(ResultSet rs, int rowNum) throws SQLException {
        List<ProdutoPedido> listaPedido = new ArrayList<>();
        ProdutoPedido pedido = new ProdutoPedido();
        pedido.setCdPedido(rs.getInt("cdPedido"));
        pedido.setCdProduto(rs.getInt("CdProduto"));
        pedido.setCdProdutoPedido(rs.getInt("CdProdutoPedido"));
        pedido.setQuantidade(rs.getInt("Quantidade"));
        pedido.setValorTotal(rs.getDouble("ValorTotal"));
        pedido.setValor(rs.getDouble("ValorUnitario"));
        pedido.setNome(rs.getString("Nome"));

        listaPedido.add(pedido);
        while(rs.next()){
            pedido = new ProdutoPedido();
            pedido.setCdPedido(rs.getInt("cdPedido"));
            pedido.setCdProduto(rs.getInt("CdProduto"));
            pedido.setCdProdutoPedido(rs.getInt("CdProdutoPedido"));
            pedido.setQuantidade(rs.getInt("Quantidade"));
            pedido.setValorTotal(rs.getDouble("ValorTotal"));
            pedido.setValor(rs.getDouble("ValorUnitario"));
            pedido.setNome(rs.getString("Nome"));
            listaPedido.add(pedido);
        }
        return listaPedido;
    }
}
