package br.com.abee.gestao.Entity;

import lombok.Data;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.LocalTime;
import java.util.Date;

@Entity
@Data
public class Despesa {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private int id;
    @Column(nullable = true)
    private String descricao;
    @Column(nullable = true)
    private Timestamp data;
    private Double valor;
}
