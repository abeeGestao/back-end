package br.com.abee.gestao.Model;

import lombok.Data;

@Data
public class FiltrosProduto {
    private int cdProduto;
    private String nome;
    private String descricao;
    private String status;
    private String cdBarras;
    private int quantidade;
    private Double valor;
    private int pagina;
    private int quantidadePagina;

}
