package br.com.abee.gestao.Repository;

import br.com.abee.gestao.Entity.Usuario;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Component
public class UsuarioMapper implements RowMapper<List<Usuario>> {
    @Override
    public List<Usuario> mapRow(ResultSet rs, int rowNum) throws SQLException {
        List<Usuario> listaUsuario = new ArrayList<>();

        Usuario usuario = new Usuario();
        usuario.setId(rs.getLong("id"));
        usuario.setNome(rs.getString("nome"));
        usuario.setNomeUsuario(rs.getString("nomeUsuario"));
        usuario.setSenha(rs.getString("senha"));
        usuario.setPerfil(rs.getInt("perfil"));
        listaUsuario.add(usuario);
        while (rs.next()) {
            usuario = new Usuario();
            usuario.setId(rs.getLong("id"));
            usuario.setNome(rs.getString("nome"));
            usuario.setNomeUsuario(rs.getString("nomeUsuario"));
            usuario.setSenha(rs.getString("senha"));
            usuario.setPerfil(rs.getInt("perfil"));
            listaUsuario.add(usuario);
        }
        return listaUsuario;
    }
}
