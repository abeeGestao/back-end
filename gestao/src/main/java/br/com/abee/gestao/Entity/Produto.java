package br.com.abee.gestao.Entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class Produto {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int cdProduto;
    private String nome;
    @Column(nullable = true)
    private String descricao;
    @Column(nullable = true)
    private String status;
    @Column(nullable = true)
    private String cdBarras;
    private int quantidade;
    private Double valor;

}
