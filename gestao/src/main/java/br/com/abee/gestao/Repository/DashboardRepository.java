package br.com.abee.gestao.Repository;

import br.com.abee.gestao.Model.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Slf4j
public class DashboardRepository {
    NamedParameterJdbcTemplate jdbcTemplate;
    DadosMapper dadosMapper;

    @Autowired
    public DashboardRepository(NamedParameterJdbcTemplate jdbcTemplate, DadosMapper dadosMapper){
        this.jdbcTemplate = jdbcTemplate;
        this.dadosMapper = dadosMapper;
    }

    public ResponseDashboard devolveDados(FiltroDados filtroDados){

        log.info("buscando Dados, Filtros:", filtroDados);
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("totalProduto", filtroDados.getTotalProduto())
                .addValue("totalCliente", filtroDados.getTotalCliente())
                .addValue("totalPedido", filtroDados.getTotalPedido())
                .addValue("totalVendido", filtroDados.getTotalValorVendido());

        String sql = "SELECT (SELECT SUM(quantidade) as total FROM PRODUTO) totalProduto,\n" +
                "(SELECT count(*) as total FROM CLIENTE) totalCliente,\n" +
                "(SELECT count(*) as total FROM PEDIDO WHERE STATUS = 'C') totalPedido,\n" +
                "(SELECT NVL(SUM(VALORTOTAL),0) TOTAL FROM  PRODUTOPEDIDO PP, PEDIDO P " +
                "WHERE PP.CDPEDIDO = P.CDPEDIDO AND P.STATUS = 'C') totalVendido from dual";
        List<Dados> listaDados = jdbcTemplate.queryForObject(sql, params, dadosMapper);
        ResponseDashboard response = new ResponseDashboard();
        response.setListaDados(listaDados);

        return response;
    }

}
