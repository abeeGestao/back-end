package br.com.abee.gestao.Util;


import javax.print.*;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.standard.JobName;
import javax.print.attribute.standard.MediaSizeName;
import javax.print.attribute.standard.OrientationRequested;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class Impressao {

    public void imprimir(String texto) throws Exception {
            //Tratar dados para impressão
            InputStream prin = new ByteArrayInputStream(texto.getBytes());
            DocFlavor docFlavor = DocFlavor.INPUT_STREAM.AUTOSENSE;
            SimpleDoc documentoTexto = new SimpleDoc(prin, docFlavor, null);

            //pega a impressora padrão
            PrintService impressora = PrintServiceLookup.lookupDefaultPrintService();

            PrintRequestAttributeSet printerAttributes = new HashPrintRequestAttributeSet();
            printerAttributes.add(new JobName("Impressao", null));
            printerAttributes.add(OrientationRequested.PORTRAIT);

            //Informa o tipo da folha, mas não adianta muita coisa
            printerAttributes.add(MediaSizeName.ISO_A4);

            DocPrintJob printJob = impressora.createPrintJob();

            //Realiza impressão
            printJob.print(documentoTexto, (PrintRequestAttributeSet) printerAttributes);

            //fecha conexão com impressora
            prin.close();
    }

        //nova versão
        private static PrintService impressora;

        public static List<String> retornaImressoras(){
                try {
                        List<String> listaImpressoras = new ArrayList<>();
                        DocFlavor df = DocFlavor.SERVICE_FORMATTED.PRINTABLE;
                        PrintService[] ps = PrintServiceLookup.lookupPrintServices(df, null);
                        for (PrintService p : ps) {
                                listaImpressoras.add(p.getName());
                        }
                        return listaImpressoras;
                } catch (Exception e) {
                        e.printStackTrace();
                }
                return null;
        }

        public void detectaImpressoras(String impressoraSelecionada) {
                try {
                        DocFlavor df = DocFlavor.SERVICE_FORMATTED.PRINTABLE;
                        PrintService[] ps = PrintServiceLookup.lookupPrintServices(df, null);
                        for (PrintService p : ps) {
                                if(p.getName()!=null && p.getName().contains(impressoraSelecionada)){
                                        impressora = p;
                                }
                        }
                } catch (Exception e) {
                        e.printStackTrace();
                }
        }

        public  boolean imprime(String texto) throws Exception {

                if (impressora == null) {
                        throw new Exception("Nenhuma impressora foi encontrada");
                } else {
                        try {
                                DocPrintJob dpj = impressora.createPrintJob();
                                InputStream stream = new ByteArrayInputStream((texto).getBytes());
                                DocFlavor flavor = DocFlavor.INPUT_STREAM.AUTOSENSE;
                                Doc doc = new SimpleDoc(stream, flavor, null);
                                dpj.print(doc, null);
                                return true;
                        } catch (PrintException e) {
                                e.printStackTrace();
                        }
                }
                return false;
        }

        public void acionarGuilhotina() throws Exception {
                imprime("1 "+(char)27+(char)109);
        }
}
