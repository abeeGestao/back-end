package br.com.abee.gestao.Rest;

import br.com.abee.gestao.Model.FiltroGrafico;
import br.com.abee.gestao.Model.ResponseCompra;
import br.com.abee.gestao.Model.ResponseGrafico;
import br.com.abee.gestao.Service.GraficoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

@RestController
@Slf4j
public class GraficoApi {

    @Autowired
    GraficoService service;

    @PostMapping("/dadosGrafico")
    @CrossOrigin(origins = "*")
    public ResponseGrafico getDevolveGrafico(@RequestBody FiltroGrafico filtroGrafico) {
        try{
            return service.getDevolveDados(filtroGrafico);
        }catch(Exception e){
            log.error("Motivo do Erro: "+e.getMessage());
            e.printStackTrace();
            ResponseGrafico responseGrafico = new ResponseGrafico();
            responseGrafico.setListaGrafico(new ArrayList<>());
            return responseGrafico;
        }
    }

}
