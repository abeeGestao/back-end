package br.com.abee.gestao.Repository;

import br.com.abee.gestao.Model.Compra;
import br.com.abee.gestao.Model.Dados;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Component
public class DadosMapper implements RowMapper<List<Dados>> {
    @Override
    public List<Dados> mapRow(ResultSet rs, int i) throws SQLException {
        List<Dados> listaDados = new ArrayList<>();
        Dados dados = new Dados();
        dados.setTotalProduto(rs.getInt("totalProduto"));
        dados.setTotalCliente(rs.getInt("totalCliente"));
        dados.setTotalPedido(rs.getInt("totalPedido"));
        dados.setTotalValorVendido(rs.getInt("totalVendido"));
        listaDados.add(dados);

        while (rs.next()){
            dados = new Dados();
            dados.setTotalProduto(rs.getInt("totalProduto"));
            dados.setTotalCliente(rs.getInt("totalCliente"));
            dados.setTotalPedido(rs.getInt("totalPedido"));
            dados.setTotalValorVendido(rs.getInt("totalVendido"));
            listaDados.add(dados);
        }

        return listaDados;
    }
}
