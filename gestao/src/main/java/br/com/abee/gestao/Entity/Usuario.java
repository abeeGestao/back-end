package br.com.abee.gestao.Entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class Usuario {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = true)
    private String nome;
    private String nomeUsuario;
    private String senha;
    private int perfil;
}
