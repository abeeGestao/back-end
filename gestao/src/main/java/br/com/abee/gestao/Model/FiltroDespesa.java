package br.com.abee.gestao.Model;

import lombok.Data;

import java.sql.Timestamp;
import java.time.LocalDate;

@Data
public class FiltroDespesa {
    private int id;
    private String descricao;
    private Timestamp dataInicio;
    private Timestamp dataFim;
    private Double valor;
    private int pagina;
    private int quantidadePagina;
}
