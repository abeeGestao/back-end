package br.com.abee.gestao.Repository;

import br.com.abee.gestao.Entity.Cliente;
import br.com.abee.gestao.Entity.Empresa;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

@Slf4j
@Repository
public class EmpresaRepository {

    NamedParameterJdbcTemplate jdbcTemplate;
    EmpresaMapper empresaMapper;

    @Autowired
    public EmpresaRepository(NamedParameterJdbcTemplate jdbcTemplate, EmpresaMapper empresaMapper){
        this.jdbcTemplate = jdbcTemplate;
        this.empresaMapper = empresaMapper;
    }

    public Empresa getEmpresa(){
        String sql = "select * from empresa";
        MapSqlParameterSource params = new MapSqlParameterSource();
        return jdbcTemplate.queryForObject(sql,params, empresaMapper);
    }

    public void atualizarCliente(Empresa empresa) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("cdEmpresa", empresa.getCdEmpresa())
                .addValue("nome", empresa.getNome())
                .addValue("documento", empresa.getDocumento())
                .addValue("razaoSocial", empresa.getRazaoSocial())
                .addValue("nomeFantasia", empresa.getNomeFantasia())
                .addValue("status", empresa.getStatus())
                .addValue("email", empresa.getEmail())
                .addValue("cep", empresa.getCep())
                .addValue("estado", empresa.getEstado())
                .addValue("pais", empresa.getPais())
                .addValue("rua", empresa.getRua())
                .addValue("bairro", empresa.getBairro())
                .addValue("numero", empresa.getNumero())
                .addValue("telefone", empresa.getTelefone())
                .addValue("cidade", empresa.getCidade());
        String sql = " update empresa " +
                " set " +
                " cep = :cep , cidade = :cidade , documento = :documento , email = :email , estado = :estado , " +
                " numero = :numero   , nome = :nome   , nomeFantasia = :nomeFantasia , pais = :pais , " +
                " bairro = :bairro   ,razaoSocial = :razaoSocial , rua = :rua , status = :status , telefone = :telefone ";
        jdbcTemplate.update(sql, params);
    }

}
