package br.com.abee.gestao.Repository;

import br.com.abee.gestao.Model.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Slf4j
public class ComprasRepository {

    NamedParameterJdbcTemplate jdbcTemplate;
    CompraMapper compraMapper;

    @Autowired
    public ComprasRepository(NamedParameterJdbcTemplate jdbcTemplate, CompraMapper compraMapper){
        this.jdbcTemplate = jdbcTemplate;
        this.compraMapper = compraMapper;
    }

    public ResponseCompra devolveCompra(FiltrosCompra filtrosCompra){

        log.info("buscando Compras, Filtros:", filtrosCompra);
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("codigo", filtrosCompra.getCodigo())
                .addValue("nome", filtrosCompra.getNome())
                .addValue("razaoSocial", filtrosCompra.getRazaoSocial())
                .addValue("total", filtrosCompra.getTotal());

        String sql = "SELECT C.CDCLIENTE as codigo, C.NOME as nome, C.RAZAOSOCIAL as razaoSocial, SUM(PP.VALORTOTAL) as total " +
                "FROM  CLIENTE C, PRODUTOPEDIDO PP, PRODUTO P, PEDIDO PE\n" +
                "WHERE C.CDCLIENTE = PE.CDCLIENTE AND P.CDPRODUTO = PP.CDPRODUTO\n" +
                "AND PE.CDPEDIDO = PP.CDPEDIDO AND PE.STATUS = 'C'\n" +
                "GROUP BY C.CDCLIENTE, C.NOME, C.RAZAOSOCIAL\n" +
                "ORDER BY TOTAL DESC";

        List<Compra> listaCompra = jdbcTemplate.queryForObject(sql, params, compraMapper);
        ResponseCompra response = new ResponseCompra();
        response.setListaCompras(listaCompra);
        return response;
    }
}
