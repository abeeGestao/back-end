package br.com.abee.gestao.Service;

import br.com.abee.gestao.Entity.Empresa;
import br.com.abee.gestao.Entity.Pedido;
import br.com.abee.gestao.Entity.ProdutoPedido;
import br.com.abee.gestao.Repository.ClienteRepository;
import br.com.abee.gestao.Repository.EmpresaRepository;
import br.com.abee.gestao.Repository.PedidoRepository;
import br.com.abee.gestao.Util.Impressao;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;


@Service
@Slf4j
public class CaixaService {
    private PedidoRepository repository;
    PedidoService pedidoService;
    EmpresaRepository empresaRepository;


    @Autowired
    public CaixaService(EmpresaRepository empresaRepository, PedidoRepository repository, PedidoService pedidoService){
        this.repository = repository;
        this.pedidoService = pedidoService;
        this.empresaRepository = empresaRepository;
    }
    public void cadastrarPedidoPdv(Pedido pedido) throws Exception {
        //calcular total da venda
        for(ProdutoPedido pe : pedido.getListaProduto()){
            pe.setValorTotal(pe.getValor() * pe.getQuantidade());
        }
        pedido.setNomeCliente("Caixa de Venda");
        pedido.setObservacao("Venda feita pelo sistema de Caixa");
        pedido.setStatus("C");
        //cadastrar pedido
        repository.cadastroPedido(pedido);
        //imprimir venda
        try{
            Impressao impressora = new Impressao();
            String cupom = montarNota(pedido);
            impressora.imprimir(cupom);
        }catch(Exception e){
            e.printStackTrace();
            log.error("Motivo: "+e.getCause());
            throw new Exception("Erro ao tentar imprimir o pedido: "+e.getMessage());
        }
    }

    //metódo para montar o cupom não fiscal
    public String montarNota(Pedido pe){
        Double valorTotal = new Double(0);
        String listaProdutoString = "" +
                "COD DESCRICAO        QTD VALOR\n\r";
        for (ProdutoPedido produto : pe.getListaProduto()) {
            valorTotal += Double.valueOf(produto.getValorTotal());
            String proString = produto.getCdProduto()+"";
            int diferenca = 5 - proString.length();
            for(int i =0; i < diferenca; i++){
                proString += " ";
            }

            String nomeString = produto.getNome()+"";
            diferenca = 16 - nomeString.length();
            for(int i =0; i < diferenca; i++){
                nomeString += " ";
            }

            String qtdString = produto.getQuantidade()+"";
            diferenca = 3 - qtdString.length();
            for(int i =0; i < diferenca; i++){
                qtdString += " ";
            }

            String valorString = produto.getValorTotal()+"";
            diferenca = 6 - valorString.length();
            for(int i =0; i < diferenca; i++){
                valorString += " ";
            }

            listaProdutoString += proString + nomeString + qtdString + valorString+"\n\r";
        }
        valorTotal = valorTotal - Double.valueOf(pe.getDesconto());
        Date dataCompra = new Date();
        String dataCompraString = new SimpleDateFormat("dd/MM/yyyy").format(dataCompra);
        Empresa empresa = empresaRepository.getEmpresa();
        String nota = "" +
                ""+empresa.getNome()+"\n\r" +
                ""+empresa.getRua()+", "+empresa.getNumero()+"\n\r"+empresa.getBairro()+
                ", "+empresa.getCidade()+" - "+empresa.getEstado()+"\n\r" +
                " Data: "+dataCompraString+"\n\r"+
                "------------------------------\n\r" +
                "        CUPOM NAO FISCAL      \n\r" +
                "------------------------------\n\r"+
                listaProdutoString+" \n\r";
                nota+=" \n\r" +
                "------------------------------\n\r"+
                "\n Valor Total: "+valorTotal.toString()+"\n\r"+
                " Valor Pago: "+pe.getValorPago()+"\n" +
                " Troco: "+(pe.getValorPago().subtract(new BigDecimal(valorTotal)))+"\n\r \n";
        return converte(nota);
    }
    public String converte(String text) {

        return text.replaceAll("[ãâàáä]", "a")
                .replaceAll("[êèéë]", "e")
                .replaceAll("[îìíï]", "i")
                .replaceAll("[õôòóö]", "o")
                .replaceAll("[ûúùü]", "u")
                .replaceAll("[ÃÂÀÁÄ]", "A")
                .replaceAll("[ÊÈÉË]", "E")
                .replaceAll("[ÎÌÍÏ]", "I")
                .replaceAll("[ÕÔÒÓÖ]", "O")
                .replaceAll("[ÛÙÚÜ]", "U")
                .replace('ç', 'c')
                .replace('Ç', 'C')
                .replace('ñ', 'n')
                .replace('Ñ', 'N');

    }
}
