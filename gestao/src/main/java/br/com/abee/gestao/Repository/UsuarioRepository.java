package br.com.abee.gestao.Repository;

import br.com.abee.gestao.Entity.Produto;
import br.com.abee.gestao.Entity.Usuario;
import br.com.abee.gestao.Model.FiltrosUsuario;
import br.com.abee.gestao.Model.ResponseUsuario;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Slf4j
public class UsuarioRepository {
    NamedParameterJdbcTemplate jdbcTemplate;
    UsuarioMapper usuarioMapper;
    QuantidadePaginaMapper quantidadePaginaMapper;

    @Autowired
    public UsuarioRepository(NamedParameterJdbcTemplate jdbcTemplate, UsuarioMapper usuarioMapper, QuantidadePaginaMapper quantidadePaginaMapper) {
        this.jdbcTemplate = jdbcTemplate;
        this.usuarioMapper = usuarioMapper;
        this.quantidadePaginaMapper = quantidadePaginaMapper;
    }

    public ResponseUsuario getUsuarios(FiltrosUsuario filtrosUsuario) {
        log.info("buscando Usuarios, Filtros:", filtrosUsuario);
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("id", filtrosUsuario.getId())
                .addValue("nome", filtrosUsuario.getNome())
                .addValue("nomeUsuario", filtrosUsuario.getNomeUsuario())
                .addValue("perfil", filtrosUsuario.getPerfil())
                .addValue("senha", filtrosUsuario.getSenha())
                .addValue("pagina", filtrosUsuario.getPagina())
                .addValue("quantidadePagina", filtrosUsuario.getQuantidadePagina());

        String sql = "select distinct * from usuario where " +
                "(id = :id or :id is null or 0 = :id )" +
                " and (nome = :nome or :nome IS null or '' = :nome )" +
                " and (nomeUsuario = :nomeUsuario  or :nomeUsuario IS null or '' = :nomeUsuario )" +
                " and (senha = :senha  or :senha IS null or '' = :senha )"+
                " and (perfil = :perfil or :perfil is null or 0 = :perfil)" +
                " order by id " +
                " offset (:pagina - 1) * :quantidadePagina rows fetch next :quantidadePagina rows only;";

        List<Usuario> listaUsuario = jdbcTemplate.queryForObject(sql, params, usuarioMapper);
        ResponseUsuario response = new ResponseUsuario();
        response.setListaUsuario(listaUsuario);
        response.setQuantidadePagina(getQuantidadeRegistro(params));
        return response;
    }

    private int getQuantidadeRegistro(MapSqlParameterSource params){
        String sql = "select count(*) as total from usuario where " +
                "(id = :id or :id is null or 0 = :id )" +
                " and (nome = :nome or :nome IS null or '' = :nome )" +
                " and (nomeUsuario = :nomeUsuario  or :nomeUsuario IS null or '' = :nomeUsuario )"+
                " and (senha = :senha  or :senha IS null or '' = :senha )"+
                "and (perfil = :perfil or :perfil is null or 0 = :perfil);";
        return jdbcTemplate.queryForObject(sql, params,quantidadePaginaMapper);
    }

    public void delete(int id) {
        log.info("Apagando usuarios");
        String sql = "delete from usuario " +
                " where :id = id ";
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("id", id);
        jdbcTemplate.update(sql, params);
        log.info("Usuario Apagado");
    }

    public void cadastroUsuario(Usuario usuario) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("nome", usuario.getNome())
                .addValue("nomeUsuario", usuario.getNomeUsuario())
                .addValue("senha", usuario.getSenha())
                .addValue("perfil", usuario.getPerfil());
        String sql = "insert into usuario\n" +
                " (nome , nomeUsuario , senha ,perfil) " +
                "values" +
                "(:nome , :nomeUsuario , :senha , :perfil);";
        jdbcTemplate.update(sql, params);
    }

    public void atualizaUsuario(Usuario usuario) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("id", usuario.getId())
                .addValue("nome", usuario.getNome())
                .addValue("nomeUsuario", usuario.getNomeUsuario())
                .addValue("senha", usuario.getSenha())
                .addValue("perfil", usuario.getPerfil());
        String sql = "update usuario\n" +
                "set nome = :nome, nomeUsuario = :nomeUsuario, senha = :senha, perfil = :perfil " +
                "where id = :id;";
        jdbcTemplate.update(sql, params);
    }

    public Usuario getUsuarioAutenticacao(FiltrosUsuario filtrosUsuario) {
        log.info("buscando Usuarios, Filtros:", filtrosUsuario);
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("nomeUsuario", filtrosUsuario.getNomeUsuario())
                .addValue("senha", filtrosUsuario.getSenha());

        String sql = "select * from usuario where " +
                " nomeUsuario = :nomeUsuario " +
                " and senha = :senha" ;

        try{
            List<Usuario> listaUsuario = jdbcTemplate.queryForObject(sql, params, usuarioMapper);
            ResponseUsuario response = new ResponseUsuario();
            return listaUsuario.get(0);
        }catch(EmptyResultDataAccessException e){
            return null;
        }

    }

}
