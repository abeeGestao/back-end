package br.com.abee.gestao.Repository;

import br.com.abee.gestao.Entity.Cliente;
import br.com.abee.gestao.Entity.Empresa;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

@Component
public class EmpresaMapper implements RowMapper<Empresa> {
    @Override
    public Empresa mapRow(ResultSet rs, int rowNum) throws SQLException {
        Empresa empresa = new Empresa();
        empresa.setCdEmpresa(rs.getInt("cdEmpresa"));
        empresa.setCep(Optional.ofNullable(rs.getString("cep")).orElse(""));
        empresa.setCidade(Optional.ofNullable(rs.getString("cidade")).orElse(""));
        empresa.setDocumento(Optional.ofNullable(rs.getString("documento")).orElse(""));
        empresa.setEmail(Optional.ofNullable(rs.getString("email")).orElse(""));
        empresa.setEstado(Optional.ofNullable(rs.getString("estado")).orElse(""));
        empresa.setNome(Optional.ofNullable(rs.getString("nome")).orElse(""));
        empresa.setNomeFantasia(Optional.ofNullable(rs.getString("nomeFantasia")).orElse(""));
        empresa.setNumero(Optional.ofNullable(rs.getString("numero")).orElse(""));
        empresa.setPais(Optional.ofNullable(rs.getString("pais")).orElse(""));
        empresa.setRazaoSocial(Optional.ofNullable(rs.getString("razaoSocial")).orElse(""));
        empresa.setRua(Optional.ofNullable(rs.getString("rua")).orElse(""));
        empresa.setBairro(Optional.ofNullable(rs.getString("bairro")).orElse(""));
        empresa.setStatus(Optional.ofNullable(rs.getString("status")).orElse(""));
        empresa.setTelefone(Optional.ofNullable(rs.getString("telefone")).orElse(""));
        return empresa;

    }
}
