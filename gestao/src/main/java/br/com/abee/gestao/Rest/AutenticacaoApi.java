package br.com.abee.gestao.Rest;

import br.com.abee.gestao.Entity.Usuario;
import br.com.abee.gestao.Model.ResultadoAutenticacao;
import br.com.abee.gestao.Service.AutenticacaoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class AutenticacaoApi {

    @Autowired
    private AutenticacaoService service;

    @PostMapping("/autenticacao")
    @CrossOrigin(origins = "*")
    public ResultadoAutenticacao autenticar(@RequestBody Usuario usuario){
        return service.autenticar(usuario);
    }
}
