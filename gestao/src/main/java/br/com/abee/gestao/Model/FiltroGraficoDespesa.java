package br.com.abee.gestao.Model;

import lombok.Data;

@Data
public class FiltroGraficoDespesa {
    private int total;
    private int hoje;
    private int um;
    private int dois;
    private int tres;
    private int quatro;
    private int cinco;
    private int seis;
}
