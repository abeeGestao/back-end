package br.com.abee.gestao.Rest;

import br.com.abee.gestao.Model.FiltrosCompra;
import br.com.abee.gestao.Model.ResponseCompra;
import br.com.abee.gestao.Service.ComprasService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
@Slf4j
public class ComprasApi {

    @Autowired
    ComprasService service;

    @PostMapping("/totalCompra")
    @CrossOrigin(origins = "*")
    public ResponseCompra getDevolveCompra(@RequestBody FiltrosCompra filtrosCompra) {
        try{
            return service.getDevolveCompra(filtrosCompra);
        }catch(Exception e){
            log.error("Motivo do Erro: "+e.getMessage());
            e.printStackTrace();
            ResponseCompra responseCompra = new ResponseCompra();
            responseCompra.setListaCompras(new ArrayList<>());
            return responseCompra;
        }
    }

}
