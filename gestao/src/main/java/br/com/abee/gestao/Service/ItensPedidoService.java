package br.com.abee.gestao.Service;

import br.com.abee.gestao.Model.FiltroItensPedido;
import br.com.abee.gestao.Model.ResponseItensPedido;
import br.com.abee.gestao.Repository.ItensPedidoRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class ItensPedidoService {
    private ItensPedidoRepository repository;

    @Autowired
    public ItensPedidoService(ItensPedidoRepository repository){
        this.repository = repository;
    }

    public ResponseItensPedido getListaItens(FiltroItensPedido filtroItensPedido) {
        return repository.devolveItens(filtroItensPedido);
    }
}
