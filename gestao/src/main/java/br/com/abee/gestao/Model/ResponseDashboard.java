package br.com.abee.gestao.Model;

import lombok.Data;

import java.util.List;

@Data
public class ResponseDashboard {
    private List<Dados> listaDados;
}
