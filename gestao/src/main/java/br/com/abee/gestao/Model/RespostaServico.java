package br.com.abee.gestao.Model;

import lombok.Data;

@Data
public class RespostaServico {
    private int codigoServico;
    private String mensagem;
}
