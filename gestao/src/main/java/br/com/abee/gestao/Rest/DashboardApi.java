package br.com.abee.gestao.Rest;

import br.com.abee.gestao.Model.FiltroDados;
import br.com.abee.gestao.Model.ResponseDashboard;
import br.com.abee.gestao.Service.DashboardService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
@Slf4j
public class DashboardApi {

    @Autowired
    DashboardService service;

    @PostMapping("/totalDados")
    @CrossOrigin(origins = "*")
    public ResponseDashboard getDevolveDados(@RequestBody FiltroDados filtroDados) {
        try{
            return service.getDevolveDados(filtroDados);
        }catch(Exception e){
            log.error("Motivo do Erro: "+e.getMessage());
            e.printStackTrace();
            ResponseDashboard responseDashboard = new ResponseDashboard();
            responseDashboard.setListaDados(new ArrayList<>());
            //responseDashboard.setComprasTotal(new ArrayList<>());
            return responseDashboard;
        }
    }

}
