package br.com.abee.gestao.Repository;

import br.com.abee.gestao.Entity.Produto;
import br.com.abee.gestao.Entity.ProdutoPedido;
import br.com.abee.gestao.Model.FiltrosProduto;
import br.com.abee.gestao.Model.ResponseDashboard;
import br.com.abee.gestao.Model.ResponseProduto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Slf4j
public class ProdutoRepository {
    NamedParameterJdbcTemplate jdbcTemplate;
    ProdutoMapper produtoMapper;
    QuantidadePaginaMapper quantidadePaginaMapper;

    @Autowired
    public ProdutoRepository(NamedParameterJdbcTemplate jdbcTemplate, ProdutoMapper produtoMapper, QuantidadePaginaMapper quantidadePaginaMapper) {
        this.jdbcTemplate = jdbcTemplate;
        this.produtoMapper = produtoMapper;
        this.quantidadePaginaMapper = quantidadePaginaMapper;
    }

    public ResponseProduto getProdutos(FiltrosProduto filtrosProduto) {
        log.info("buscando Produtos, Filtros:", filtrosProduto);
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("cdProduto", filtrosProduto.getCdProduto())
                .addValue("nome", filtrosProduto.getNome())
                .addValue("cdBarras", filtrosProduto.getCdBarras())
                .addValue("descricao", filtrosProduto.getDescricao())
                .addValue("quantidade", filtrosProduto.getQuantidade())
                .addValue("status", filtrosProduto.getStatus())
                .addValue("valor", filtrosProduto.getValor())
                .addValue("pagina", filtrosProduto.getPagina())
                .addValue("quantidadePagina", filtrosProduto.getQuantidadePagina());

        String sql = "select * from produto where " +
                "(cdProduto = :cdProduto or :cdProduto is null or 0 = :cdProduto )" +
                " and (nome = :nome or :nome IS null or '' = :nome )" +
                " and (descricao = :descricao  or :descricao IS null or '' = :descricao )" +
                " and (cdBarras = :cdBarras  or :cdBarras IS null or '' = :cdBarras )" +
                " and (quantidade = :quantidade  or :quantidade IS null or 0 = :quantidade )" +
                " and (status = :status  or :status IS null or '' = :status )" +
                " and (valor = :valor or :valor IS null or 0 = :valor )" +
                " order by cdProduto DESC" +
                " offset (:pagina - 1) * :quantidadePagina rows fetch next :quantidadePagina rows only;";
        List<Produto> listaProduto = jdbcTemplate.queryForObject(sql, params, produtoMapper);
        ResponseProduto response = new ResponseProduto();
        response.setListaProduto(listaProduto);
        response.setQuantidadePagina(getQuantidadeRegistro(params));
        return response;
    }

    private int getQuantidadeRegistro(MapSqlParameterSource params){
        String sql = "select count(*) as total from produto where " +
                "(cdProduto = :cdProduto or :cdProduto is null or 0 = :cdProduto )" +
                " and (nome = :nome or :nome IS null or '' = :nome )" +
                " and (descricao = :descricao  or :descricao IS null or '' = :descricao )" +
                " and (cdBarras = :cdBarras  or :cdBarras IS null or '' = :cdBarras )" +
                " and (quantidade = :quantidade  or :quantidade IS null or 0 = :quantidade )" +
                " and (status = :status  or :status IS null or '' = :status )" +
                " and (valor = :valor or :valor IS null or 0 = :valor );";
        return jdbcTemplate.queryForObject(sql, params,quantidadePaginaMapper);
    }

    public void delete(int cdProduto) {
        log.info("Apagando produtos");
        String sql = "delete from produto " +
                " where :cdProduto = cdProduto ";
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("cdProduto", cdProduto);
        jdbcTemplate.update(sql, params);
        log.info("Produto Apagado");
    }

    public void cadastroProduto(Produto produto) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("nome", produto.getNome())
                .addValue("descricao", produto.getDescricao())
                .addValue("cdBarras", produto.getCdBarras())
                .addValue("quantidade", produto.getQuantidade())
                .addValue("status", produto.getStatus())
                .addValue("valor", produto.getValor());
        String sql = "insert into produto\n" +
                " (nome , descricao , cdBarras ,quantidade, status, valor) " +
                "values" +
                "(:nome , :descricao , :cdBarras , :quantidade , 'A' , :valor);";
        jdbcTemplate.update(sql, params);
    }

    public void atualizaProduto(Produto produto){
        log.info("Atualizando produtos");
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("cdProduto", produto.getCdProduto())
                .addValue("nome", produto.getNome())
                .addValue("descricao", produto.getDescricao())
                .addValue("cdBarras", produto.getCdBarras())
                .addValue("quantidade", produto.getQuantidade())
                .addValue("status", produto.getStatus())
                .addValue("valor", produto.getValor());
        String sql = "update produto\n" +
                " set nome = :nome , descricao = :descricao , cdBarras = :cdBarras ,quantidade = :quantidade, " +
                "status = :status, valor = :valor " +
                "where cdProduto = :cdProduto;";
        jdbcTemplate.update(sql, params);
    }

    public void atualizaEstoqueProduto(ProdutoPedido produto){
        log.info("Atualizando produtos");
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("cdProduto", produto.getCdProduto())
                .addValue("quantidade", produto.getQuantidade());

        String sql = "update produto\n" +
                " set quantidade = (quantidade - :quantidade) " +
                " where cdProduto = :cdProduto;";
        jdbcTemplate.update(sql, params);
    }

    public ResponseProduto getAllProdutos() {
        MapSqlParameterSource params = new MapSqlParameterSource();
        String sql = "select * from produto";
        List<Produto> listaProduto = jdbcTemplate.queryForObject(sql, params, produtoMapper);
        ResponseProduto response = new ResponseProduto();
        response.setListaProduto(listaProduto);
        return response;
    }

}
