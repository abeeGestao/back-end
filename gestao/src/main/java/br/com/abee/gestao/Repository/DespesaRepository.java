package br.com.abee.gestao.Repository;

import br.com.abee.gestao.Entity.Despesa;
import br.com.abee.gestao.Entity.Usuario;
import br.com.abee.gestao.Model.FiltroDespesa;
import br.com.abee.gestao.Model.FiltrosUsuario;
import br.com.abee.gestao.Model.ResponseDespesa;
import br.com.abee.gestao.Model.ResponseUsuario;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Slf4j
public class DespesaRepository {
    NamedParameterJdbcTemplate jdbcTemplate;
    DespesaMapper despesaMapper;
    QuantidadePaginaMapper quantidadePaginaMapper;

    @Autowired
    public DespesaRepository(NamedParameterJdbcTemplate jdbcTemplate, DespesaMapper despesaMapper, QuantidadePaginaMapper quantidadePaginaMapper) {
        this.jdbcTemplate = jdbcTemplate;
        this.despesaMapper = despesaMapper;
        this.quantidadePaginaMapper = quantidadePaginaMapper;
    }

    public ResponseDespesa getDespesa(FiltroDespesa filtroDespesa) {
        String datainicio = null;
        if(filtroDespesa.getDataInicio() != null){datainicio = filtroDespesa.getDataInicio().toString();}
        String datafim = null;
        if(filtroDespesa.getDataInicio() != null){
            //add 1 dia
            long oneDay = 1 * 24 * 60 * 60 * 1000;
            filtroDespesa.getDataFim().setTime((filtroDespesa.getDataFim().getTime()+oneDay));
            datafim = filtroDespesa.getDataFim().toString();
        }


        log.info("buscando Despesas, Filtros:", filtroDespesa);
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("id", filtroDespesa.getId())
                .addValue("descricao", filtroDespesa.getDescricao())
                .addValue("dataInicio", datainicio)
                .addValue("dataFim", datafim)
                .addValue("valor", filtroDespesa.getValor())
                .addValue("pagina", filtroDespesa.getPagina())
                .addValue("quantidadePagina", filtroDespesa.getQuantidadePagina());

        String sql = "select  * from despesa where " +
                "(id = :id or :id is null or 0 = :id )" +
                " and (descricao = :descricao or :descricao IS null or '' = :descricao )" +
                " and (data > :dataInicio  or :dataInicio IS null or '' = :dataInicio )" +
                " and (data < :dataFim  or :dataFim IS null or '' = :dataFim )" +
                " and (valor = :valor  or :valor IS null or '' = :valor )"+
                " order by id " +
                " offset (:pagina - 1) * :quantidadePagina rows fetch next :quantidadePagina rows only;";

        List<Despesa> listaDespesa = jdbcTemplate.queryForObject(sql, params, despesaMapper);
        ResponseDespesa response = new ResponseDespesa();
        response.setListaDespesa(listaDespesa);
        response.setQuantidadePagina(getQuantidadeRegistro(params));
        return response;
    }

    private int getQuantidadeRegistro(MapSqlParameterSource params){
        String sql = "select count(*) as total from despesa where " +
                "(id = :id or :id is null or 0 = :id )" +
                " and (descricao = :descricao or :descricao IS null or '' = :descricao )" +
                " and (data > :dataInicio  or :dataInicio IS null or '' = :dataInicio )" +
                " and (data < :dataFim  or :dataFim IS null or '' = :dataFim )" +
                " and (valor = :valor  or :valor IS null or '' = :valor );";

        return jdbcTemplate.queryForObject(sql, params,quantidadePaginaMapper);
    }

    public void delete(int id) {
        log.info("Apagando despesas");
        String sql = "delete from despesa " +
                " where :id = id ";
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("id", id);
        jdbcTemplate.update(sql, params);
        log.info("Despesa Apagada");
    }

    public void cadastroDespesa(Despesa despesa) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("descricao", despesa.getDescricao())
                .addValue("data", despesa.getData())
                .addValue("valor", despesa.getValor());
        String sql = "insert into despesa\n" +
                " (descricao , data , valor) " +
                "values" +
                "(:descricao , sysdate , :valor);";
        jdbcTemplate.update(sql, params);
    }

    public void atualizaDespesa(Despesa despesa) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("id", despesa.getId())
                .addValue("descricao", despesa.getDescricao())
                .addValue("valor", despesa.getValor());
        String sql = "update despesa\n" +
                "set descricao = :descricao, valor = :valor " +
                "where id = :id;";
        jdbcTemplate.update(sql, params);
    }
}
