package br.com.abee.gestao.Rest;


import br.com.abee.gestao.Entity.Produto;
import br.com.abee.gestao.Model.FiltrosProduto;

import br.com.abee.gestao.Model.ResponseProduto;
import br.com.abee.gestao.Model.RespostaServico;
import br.com.abee.gestao.Service.ProdutoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
@Slf4j
public class ProdutoApi {
    @Autowired
    ProdutoService service;

    @PostMapping("getProdutos")
    @CrossOrigin(origins = "*")
    public ResponseProduto getProdutos(@RequestBody FiltrosProduto filtrosProduto) {
        try{
            return service.getProdutos(filtrosProduto);
        }catch(Exception e){
            log.error("Motivo do Erro: "+e.getMessage());
            e.printStackTrace();
            ResponseProduto responseProduto = new ResponseProduto();
            responseProduto.setListaProduto(new ArrayList<>());
            responseProduto.setQuantidadePagina(0);
            return responseProduto;
        }
    }

    @GetMapping("getAllProdutos")
    @CrossOrigin(origins = "*")
    public ResponseProduto getAllProdutos() {
        try{
            return service.getAllProdutos();
        }catch(Exception e){
            log.error("Motivo do Erro: "+e.getMessage());
            e.printStackTrace();
            ResponseProduto responseProduto = new ResponseProduto();
            responseProduto.setListaProduto(new ArrayList<>());
            responseProduto.setQuantidadePagina(0);
            return responseProduto;
        }
    }

    @DeleteMapping("/deleteProduto/{id}")
    @CrossOrigin(origins = "*")
    public ResponseEntity<RespostaServico> deleteProdutos(@PathVariable("id") int cdProduto){
        RespostaServico respostaServico = new RespostaServico();
        try{
            service.delete(cdProduto);
            respostaServico.setCodigoServico(0);
            respostaServico.setMensagem("Sucesso ao apagar o produto");
            return new ResponseEntity<RespostaServico>(respostaServico, HttpStatus.OK);
        }catch(Exception e){
            respostaServico.setCodigoServico(1);
            respostaServico.setMensagem("Erro ao tentar apagar o produto");
            return new ResponseEntity<RespostaServico>(respostaServico, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/cadastroProduto")
    @CrossOrigin(origins = "*")
    public ResponseEntity<RespostaServico> cadastroProduto(@RequestBody Produto produto){
        RespostaServico respostaServico = new RespostaServico();
        try{
            service.cadastroProduto(produto);
            respostaServico.setCodigoServico(0);
            respostaServico.setMensagem("Produto Cadastrado com sucesso");
            return new ResponseEntity<RespostaServico>(respostaServico, HttpStatus.OK);
        }catch(Exception e){
            log.error("Motivo do Erro: "+e.getMessage());
            e.printStackTrace();
            respostaServico.setCodigoServico(1);
            respostaServico.setMensagem("Erro ao tentar cadastrar o produto, motivo:"+e.getMessage());
            return new ResponseEntity<RespostaServico>(respostaServico, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/atualizaProduto")
    @CrossOrigin(origins = "*")
    public ResponseEntity<RespostaServico> atualizaProduto(@RequestBody Produto produto){
        RespostaServico respostaServico = new RespostaServico();
        try{
            service.atualizaProduto(produto);
            respostaServico.setCodigoServico(0);
            respostaServico.setMensagem("Produto Atualizado com sucesso");
            return  new ResponseEntity<RespostaServico>(respostaServico, HttpStatus.OK);
        }catch (Exception e){
            log.error("Motivo do Erro: " + e.getMessage());
            e.printStackTrace();
            respostaServico.setCodigoServico(1);
            respostaServico.setMensagem("Erro ao tentar atualizar o produto, motivo: " + e.getMessage());
            return  new ResponseEntity<RespostaServico>(respostaServico, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
