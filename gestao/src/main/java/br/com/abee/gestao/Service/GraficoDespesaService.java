package br.com.abee.gestao.Service;

import br.com.abee.gestao.Model.*;
import br.com.abee.gestao.Repository.GraficoDespesaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GraficoDespesaService {
    @Autowired
    GraficoDespesaRepository repository;


    public ResponseGraficoDespesa getDevolveDados(FiltroGraficoDespesa filtroGrafico) {
        return repository.devolveDados(filtroGrafico);
    }
}
