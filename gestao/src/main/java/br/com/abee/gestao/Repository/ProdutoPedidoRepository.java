package br.com.abee.gestao.Repository;
import br.com.abee.gestao.Entity.Pedido;
import br.com.abee.gestao.Entity.ProdutoPedido;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Slf4j
public class ProdutoPedidoRepository {
    NamedParameterJdbcTemplate jdbcTemplate;
    ProdutoPedidoMapper produtoPedidoMapper;
    QuantidadePaginaMapper quantidadePaginaMapper;
    ProdutoRepository produtoRepository;

    @Autowired
    public ProdutoPedidoRepository(ProdutoRepository produtoRepository, NamedParameterJdbcTemplate jdbcTemplate, ProdutoPedidoMapper produtoPedidoMapper, QuantidadePaginaMapper quantidadePaginaMapper){
        this.jdbcTemplate = jdbcTemplate;
        this.produtoPedidoMapper = produtoPedidoMapper;
        this.quantidadePaginaMapper = quantidadePaginaMapper;
        this.produtoRepository = produtoRepository;
    }

    public void cadastroProdutoPedido(ProdutoPedido produtoPedido){
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("CDPEDIDO",produtoPedido.getCdPedido())
            .addValue("CDPRODUTO",produtoPedido.getCdProduto())
            .addValue("QUANTIDADE",produtoPedido.getQuantidade())
            .addValue("VALORTOTAL",produtoPedido.getValorTotal())
            .addValue("VALORUNITARIO",produtoPedido.getValor())
            .addValue("DESCRICAO", produtoPedido.getDescricao())
            .addValue("NOME", produtoPedido.getNome());

        String sql = "insert into produtopedido " +
                "(CDPEDIDO ,CDPRODUTO ,QUANTIDADE ,VALORTOTAL ,VALOR, DESCRICAO, NOME) " +
                "values " +
                "(:CDPEDIDO ,:CDPRODUTO ,:QUANTIDADE ,:VALORTOTAL ,:VALORUNITARIO ,:DESCRICAO, :NOME)";
        jdbcTemplate.update(sql, params);
        produtoRepository.atualizaEstoqueProduto(produtoPedido);
    }


}
