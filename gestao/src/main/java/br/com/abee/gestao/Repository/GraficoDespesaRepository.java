package br.com.abee.gestao.Repository;

import br.com.abee.gestao.Model.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Slf4j
public class GraficoDespesaRepository {
    NamedParameterJdbcTemplate jdbcTemplate;
    GraficoDespesaMapper graficoMapper;

    @Autowired
    public GraficoDespesaRepository(NamedParameterJdbcTemplate jdbcTemplate, GraficoDespesaMapper graficoMapper){
        this.jdbcTemplate = jdbcTemplate;
        this.graficoMapper = graficoMapper;
    }

    public ResponseGraficoDespesa devolveDados(FiltroGraficoDespesa filtroGrafico){

        log.info("buscando Dados, Filtros:", filtroGrafico);
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("total", filtroGrafico.getTotal())
                .addValue("hoje", filtroGrafico.getHoje())
                .addValue("um", filtroGrafico.getUm())
                .addValue("dois", filtroGrafico.getDois())
                .addValue("tres", filtroGrafico.getTres())
                .addValue("quatro", filtroGrafico.getQuatro())
                .addValue("cinco", filtroGrafico.getCinco())
                .addValue("seis", filtroGrafico.getSeis());

        String sql = "SELECT \n" +
                "(select sum(valor) from despesa p where trunc(p.data) between trunc(sysdate) - 5 and trunc(sysdate) ) total,\n" +
                "(select sum(valor) from despesa p where trunc(p.data) = trunc(sysdate) )  hoje,\n" +
                "(select sum(valor) from despesa p where trunc(p.data) = trunc(sysdate) - 1 )  um,\n" +
                "(select sum(valor) from despesa p where trunc(p.data) = trunc(sysdate) - 2 )  dois,\n" +
                "(select sum(valor) from despesa p where trunc(p.data) = trunc(sysdate) - 3 )  tres,\n" +
                "(select sum(valor) from despesa p where trunc(p.data) = trunc(sysdate) - 4 )  quatro,\n" +
                "(select sum(valor) from despesa p where trunc(p.data) = trunc(sysdate) - 5 )  cinco,\n" +
                "(select sum(valor) from despesa p where trunc(p.data) = trunc(sysdate) - 6 )  seis,\n" +
                "FROM dual";
        List<GraficoDespesa> listaGrafico = jdbcTemplate.queryForObject(sql, params, graficoMapper);
        ResponseGraficoDespesa response = new ResponseGraficoDespesa();
        response.setListaGrafico(listaGrafico);

        return response;
    }
}
