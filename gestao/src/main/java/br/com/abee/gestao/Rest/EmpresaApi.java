package br.com.abee.gestao.Rest;

import br.com.abee.gestao.Entity.Empresa;
import br.com.abee.gestao.Model.RespostaServico;
import br.com.abee.gestao.Service.EmpresaService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@Slf4j
public class EmpresaApi {

    EmpresaService service;
    @Autowired
    public EmpresaApi(EmpresaService service){
        this.service = service;
    }

    @GetMapping("/getEmpresa")
    @CrossOrigin(origins = "*")
    public Empresa getEmpresa(){
        return service.getEmpresa();
    }

    @PutMapping("/putEmpresa")
    @CrossOrigin(origins = "*")
    public ResponseEntity<RespostaServico> putEmpresa(@RequestBody Empresa empresa){
        RespostaServico respostaServico = new RespostaServico();
        try{
            service.atualizarEmpresa(empresa);
            respostaServico.setCodigoServico(0);
            respostaServico.setMensagem("Dados Atualizados com sucesso");
            return new ResponseEntity<RespostaServico>(respostaServico, HttpStatus.OK);
        }catch(Exception e){
            log.error("Motivo do Erro: "+e.getMessage());
            e.printStackTrace();
            respostaServico.setCodigoServico(1);
            respostaServico.setMensagem("Erro ao tentar atualizar os dados, motivo:"+e.getMessage());
            return new ResponseEntity<RespostaServico>(respostaServico, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}