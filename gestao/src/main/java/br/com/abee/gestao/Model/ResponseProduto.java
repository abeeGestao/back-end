package br.com.abee.gestao.Model;

import br.com.abee.gestao.Entity.Produto;
import lombok.Data;

import java.util.List;

@Data
public class ResponseProduto {
    private int quantidadePagina;
    private List<Produto> listaProduto;
}
