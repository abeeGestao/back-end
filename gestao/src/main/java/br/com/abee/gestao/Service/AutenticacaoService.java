package br.com.abee.gestao.Service;

import br.com.abee.gestao.Entity.Usuario;
import br.com.abee.gestao.Model.FiltrosUsuario;
import br.com.abee.gestao.Model.ResultadoAutenticacao;
import br.com.abee.gestao.Repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AutenticacaoService {
    @Autowired
    UsuarioRepository repository;
    public ResultadoAutenticacao autenticar(Usuario usuario) {
        FiltrosUsuario filtrosUsuario = new FiltrosUsuario();
        filtrosUsuario.setNomeUsuario(usuario.getNomeUsuario());
        filtrosUsuario.setSenha(usuario.getSenha());
        Usuario userAuth = repository.getUsuarioAutenticacao(filtrosUsuario);
        if(userAuth != null){
            return userToAuth(userAuth);
        }else{
            ResultadoAutenticacao notAuth = new ResultadoAutenticacao();
            notAuth.setAutenticado(false);
            return notAuth;
        }

    }

    private ResultadoAutenticacao userToAuth(Usuario user){
        ResultadoAutenticacao autenticado = new ResultadoAutenticacao();
        autenticado.setAutenticado(true);
        autenticado.setNome(user.getNome());
        autenticado.setPerfil(user.getPerfil());
        autenticado.setUsuario(user.getNomeUsuario());
        return autenticado;
    }
}
