package br.com.abee.gestao.Entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class Empresa {
    @Id
    private int CdEmpresa;
    @Column(nullable = true)
    private String nome;
    @Column(nullable = true)
    private String documento;
    @Column(nullable = true)
    private String razaoSocial;
    @Column(nullable = true)
    private String nomeFantasia;
    @Column(nullable = true)
    private String status;
    @Column(nullable = true)
    private String email;
    @Column(nullable = true)
    private String telefone;
    @Column(nullable = true)
    private String cep;
    @Column(nullable = true)
    private String rua;
    @Column(nullable = true)
    private String numero;
    @Column(nullable = true)
    private String cidade;
    @Column(nullable = true)
    private String bairro;
    @Column(nullable = true)
    private String estado;
    @Column(nullable = true)
    private String pais;

}
