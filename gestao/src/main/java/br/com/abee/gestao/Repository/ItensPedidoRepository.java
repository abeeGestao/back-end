package br.com.abee.gestao.Repository;

import br.com.abee.gestao.Entity.Cliente;
import br.com.abee.gestao.Model.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Slf4j
public class ItensPedidoRepository {

    NamedParameterJdbcTemplate jdbcTemplate;
    ItensMapper itensMapper;
    QuantidadePaginaMapper quantidadePaginaMapper;
    ClienteRepository clienteRepository;

    @Autowired
    public ItensPedidoRepository(ClienteRepository clienteRepository, NamedParameterJdbcTemplate jdbcTemplate, ItensMapper itensMapper, QuantidadePaginaMapper quantidadePaginaMapper) {
        this.jdbcTemplate = jdbcTemplate;
        this.itensMapper = itensMapper;
        this.quantidadePaginaMapper = quantidadePaginaMapper;
        this.clienteRepository = clienteRepository;
    }

    public ResponseItensPedido devolveItens(FiltroItensPedido filtroItensPedido){

        log.info("buscando Itens, Filtros:", filtroItensPedido);

        if (filtroItensPedido.getPagina() == 0){
            filtroItensPedido.setPagina(1);
        }

        if (filtroItensPedido.getQuantidadePagina() == 0){
            filtroItensPedido.setQuantidadePagina(5);
        }

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("cdPedido", filtroItensPedido.getCdPedido())
                .addValue("codigo", filtroItensPedido.getCodigo())
                .addValue("nome", filtroItensPedido.getNome())
                .addValue("quantidade", filtroItensPedido.getQuantidade())
                .addValue("valor", filtroItensPedido.getValor())
                .addValue("total", filtroItensPedido.getTotal())
                .addValue("observacao", filtroItensPedido.getObservacao())
                .addValue("cliente", filtroItensPedido.getCliente())
                .addValue("pagamento", filtroItensPedido.getPagamento())
                .addValue("data", filtroItensPedido.getData())
                .addValue("pagina", filtroItensPedido.getPagina())
                .addValue("quantidadePagina", filtroItensPedido.getQuantidadePagina());

        String sql = "SELECT PP.CDPEDIDO, P.CDPRODUTO AS CODIGO, P.NOME AS NOME, P.VALOR AS VALOR, PP.QUANTIDADE, \n" +
                "(PP.QUANTIDADE * P.VALOR) AS TOTAL, PE.OBSERVACAO OBSERVACAO,\n" +
                "NVL((SELECT C.CDCLIENTE FROM CLIENTE C WHERE C.CDCLIENTE = PE.CDCLIENTE),'0') CLIENTE,\n" +
                "PE.FORMAPAGAMENTO PAGAMENTO,\n" +
                "PE.DATAPEDIDO DATA, PE.STATUS STATUS \n" +
                "FROM PRODUTOPEDIDO PP, PRODUTO P, PEDIDO PE\n" +
                " WHERE PP.CDPRODUTO = P.CDPRODUTO AND PE.CDPEDIDO = PP.CDPEDIDO" +
                " and (PP.CDPEDIDO  = :cdPedido or :cdPedido is null or 0 = :cdPedido )" +
                " order by P.NOME" +
                " offset (:pagina - 1) * :quantidadePagina rows fetch next :quantidadePagina rows only;\n";

        List<ItensPedido> listaItens = jdbcTemplate.queryForObject(sql, params, itensMapper);
        listaItens = this.getCliente(listaItens);
        ResponseItensPedido response = new ResponseItensPedido();
        response.setListaItens(listaItens);
        response.setQuantidadePagina(getQuantidadeRegistro(params));
        return response;
    }

    private int getQuantidadeRegistro(MapSqlParameterSource params){
        String sql = "SELECT count(pp.CDPEDIDO) as total  \n" +
                "FROM PRODUTOPEDIDO PP, PRODUTO P, PEDIDO PE\n" +
                " WHERE PP.CDPRODUTO = P.CDPRODUTO AND PE.CDPEDIDO = PP.CDPEDIDO \n" +
                " and (PP.CDPEDIDO  = :cdPedido or :cdPedido is null or 0 = :cdPedido );\n";
        return jdbcTemplate.queryForObject(sql, params,quantidadePaginaMapper);
    }
    private List<ItensPedido> getCliente( List<ItensPedido> itens){
        if(itens.get(0).getCliente().equals("0")){
            Cliente cliente = new Cliente();
            String caixa = "Compra realizada pelo Caixa";
            cliente.setNome(caixa);
            cliente.setDocumento(caixa);
            cliente.setEmail(caixa);
            itens.get(0).setClienteObjeto(cliente);
            return itens;
        }
        FiltrosCliente filtro = new FiltrosCliente();
        filtro.setCdCliente(Integer.valueOf(itens.get(0).getCliente()));
        ResponseCliente result = clienteRepository.getClientes(filtro);
        itens.get(0).setClienteObjeto(result.getListaCliente().get(0));
        return itens;
    }
}
