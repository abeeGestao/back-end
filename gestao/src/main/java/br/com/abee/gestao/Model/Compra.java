package br.com.abee.gestao.Model;

import lombok.Data;

@Data
public class Compra {
    private int codigo;
    private String nome;
    private String razaoSocial;
    private float total;
}
