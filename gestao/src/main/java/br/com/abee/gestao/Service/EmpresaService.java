package br.com.abee.gestao.Service;

import br.com.abee.gestao.Entity.Empresa;
import br.com.abee.gestao.Repository.EmpresaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmpresaService {
    private EmpresaRepository repository;

    @Autowired
    public EmpresaService(EmpresaRepository repository){
        this.repository = repository;
    }

    public Empresa getEmpresa(){
        return repository.getEmpresa();
    }

    public void atualizarEmpresa(Empresa empresa){
        repository.atualizarCliente(empresa);
    }
}
