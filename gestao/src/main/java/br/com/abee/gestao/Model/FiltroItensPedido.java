package br.com.abee.gestao.Model;

import lombok.Data;

import java.sql.Timestamp;

@Data
public class FiltroItensPedido {
    private int cdPedido;
    private int codigo;
    private String nome;
    private float valor;
    private int quantidade;
    private float total;
    private String observacao;
    private String cliente;
    private String pagamento;
    private Timestamp data;
    private int pagina;
    private int quantidadePagina;
}
