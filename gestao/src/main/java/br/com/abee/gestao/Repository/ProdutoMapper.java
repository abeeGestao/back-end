package br.com.abee.gestao.Repository;

import br.com.abee.gestao.Entity.Produto;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Component
public class ProdutoMapper implements RowMapper<List<Produto>> {

    @Override
    public List<Produto> mapRow(ResultSet rs, int rowNum) throws SQLException {
        List<Produto> listaProduto = new ArrayList<>();

        Produto produto = new Produto();
        produto.setCdProduto(rs.getInt("cdProduto"));
        produto.setCdBarras(rs.getString("cdBarras"));
        produto.setDescricao(rs.getString("descricao"));
        produto.setNome(rs.getString("nome"));
        produto.setStatus(rs.getString("status"));
        produto.setQuantidade(rs.getInt("quantidade"));
        produto.setValor(rs.getDouble("valor"));
        listaProduto.add(produto);
        while (rs.next()) {
            produto = new Produto();
            produto.setCdProduto(rs.getInt("cdProduto"));
            produto.setCdBarras(rs.getString("cdBarras"));
            produto.setDescricao(rs.getString("descricao"));
            produto.setNome(rs.getString("nome"));
            produto.setStatus(rs.getString("status"));
            produto.setQuantidade(rs.getInt("quantidade"));
            produto.setValor(rs.getDouble("valor"));
            listaProduto.add(produto);
        }
        return listaProduto;
    }

}
