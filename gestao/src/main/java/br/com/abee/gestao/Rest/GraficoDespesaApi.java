package br.com.abee.gestao.Rest;

import br.com.abee.gestao.Model.*;
import br.com.abee.gestao.Service.GraficoDespesaService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

@RestController
@Slf4j
public class GraficoDespesaApi {
    @Autowired
    GraficoDespesaService service;

    @PostMapping("/dadosGraficoDespesa")
    @CrossOrigin(origins = "*")
    public ResponseGraficoDespesa getDevolveGrafico(@RequestBody FiltroGraficoDespesa filtroGrafico) {
        try{
            return service.getDevolveDados(filtroGrafico);
        }catch(Exception e){
            log.error("Motivo do Erro: "+e.getMessage());
            e.printStackTrace();
            ResponseGraficoDespesa responseGrafico = new ResponseGraficoDespesa();
            responseGrafico.setListaGrafico(new ArrayList<>());
            return responseGrafico;
        }
    }
}
