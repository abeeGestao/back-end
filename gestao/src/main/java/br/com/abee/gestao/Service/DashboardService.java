package br.com.abee.gestao.Service;

import br.com.abee.gestao.Model.FiltroDados;
import br.com.abee.gestao.Model.ResponseDashboard;
import br.com.abee.gestao.Repository.DashboardRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DashboardService {

    @Autowired
    DashboardRepository repository;


    public ResponseDashboard getDevolveDados(FiltroDados filtroDados) {
        return repository.devolveDados(filtroDados);
    }

}
