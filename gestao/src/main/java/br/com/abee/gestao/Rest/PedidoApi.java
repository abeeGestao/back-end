package br.com.abee.gestao.Rest;

import br.com.abee.gestao.Entity.Pedido;
import br.com.abee.gestao.Model.FiltrosPedido;
import br.com.abee.gestao.Model.ResponseListaPedido;
import br.com.abee.gestao.Model.RespostaServico;
import br.com.abee.gestao.Service.PedidoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.ArrayList;

@RestController
@Slf4j
public class PedidoApi {

    @Autowired
    PedidoService service;

    @PostMapping("getPedidos")
    @CrossOrigin(origins = "*")
    public ResponseListaPedido getListaPedido(@RequestBody FiltrosPedido filtrosPedido){

        try{
            return service.getPedidos(filtrosPedido);
        }catch (Exception e){
            e.printStackTrace();
            log.error("Erro ao buscar pedidos: "+e.getMessage());
            ResponseListaPedido pedidoResponse = new ResponseListaPedido();
            pedidoResponse.setListaPedido(new ArrayList<>());
            pedidoResponse.setQuantidadePagina(0);
            return  pedidoResponse;
        }
    }

    @DeleteMapping("/deletePedido/{id}")
    @CrossOrigin(origins = "*")
    public ResponseEntity<RespostaServico> deletePedido(@PathVariable("id") int cdPedido){
        try{
            service.delete(cdPedido);
            RespostaServico respostaServico = new RespostaServico();
            respostaServico.setCodigoServico(0);
            respostaServico.setMensagem("Sucesso ao apagar o Pedido");
            return new ResponseEntity<RespostaServico>(respostaServico, HttpStatus.OK);
        }catch(Exception e){
            RespostaServico respostaServico = new RespostaServico();
            respostaServico.setCodigoServico(1);
            respostaServico.setMensagem("Erro ao tentar apagar o Pedido");
            return new ResponseEntity<RespostaServico>(respostaServico, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/cadastroPedido")
    @CrossOrigin(origins = "*")
    public ResponseEntity<RespostaServico> cadastroPedido(@RequestBody Pedido pedido){
        RespostaServico respostaServico = new RespostaServico();
        try{
            service.cadastroPedido(pedido);
            respostaServico.setCodigoServico(0);
            respostaServico.setMensagem("Pedido Cadastrado com sucesso");
            return new ResponseEntity<RespostaServico>(respostaServico, HttpStatus.OK);
        }catch(Exception e){
            log.error("Motivo do Erro: "+e.getMessage());
            e.printStackTrace();
            respostaServico.setCodigoServico(1);
            respostaServico.setMensagem("Erro ao tentar cadastrar o Pedido, motivo:"+e.getMessage());
            return new ResponseEntity<RespostaServico>(respostaServico, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @PutMapping("/confirmarPedido")
    @CrossOrigin(origins = "*")
    public ResponseEntity<RespostaServico> confirmarPedido(@RequestBody Pedido pedido){
        RespostaServico respostaServico = new RespostaServico();
        try{
            service.confirmarPedido(pedido);
            respostaServico.setCodigoServico(0);
            respostaServico.setMensagem("Pedido Cadastrado com sucesso");
            return new ResponseEntity<RespostaServico>(respostaServico, HttpStatus.OK);
        }catch(Exception e){
            log.error("Motivo do Erro: "+e.getMessage());
            e.printStackTrace();
            respostaServico.setCodigoServico(1);
            respostaServico.setMensagem("Erro ao tentar cadastrar o Pedido, motivo:"+e.getMessage());
            return new ResponseEntity<RespostaServico>(respostaServico, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
